# EmacsInGT
A (currently alpha WiP) re-implementation of Emacs targeting [GToolkit](https://gtoolkit.com/).

## Installation
- This package should be installable via the `Gt4Git` tool (see notes).
- It can also be installed by executing in a Playground:
```
Metacello new
    baseline: 'EmacsInGT';
    repository: 'gitlab://skeledrew/EmacsInGT:master/src';
    load.
```
- Create a file `data.txt` in `pharo-local` and add:
  + `emacsclient <path-to-client-binary>`
  + `lispDirs <path-to-repo-lisp-dir>`

## Usage
- Ensure there is an instance of Emacs running in server mode.
- Evaluate in Playground:
```
EigtEngine new
    loadLisp;  "can take a while the first time (result is cached)"
    read: '(prog2 5 3 (print "hello"))';  "expression to evaluate"
    eval;
    print.
```
- ELisp objects can also be explored by inspecting the result of `... read` or `... eval`.

## Development
- Ensure project dependencies are loaded.
- Clone the repo and load via `Gt4Git`.
- Ensure `data.txt` as described under *Installation* exists.
- Run parser examples with `ELispParserExamples runAllExamples reject: [:example | example isSuccess].`.
- Run native engine examples with `(EigtEngineExamples runExamplesIn: #readEvalPrint:andExpect:) reject: [:res | res isSuccess].`.
- Start debugging by sending `debug` to the run result collection.
- Have fun implementing/fixing stuff :).

## Notes
- This project depends on [GtExemplifierAdditions](https://gitlab.com/skeledrew/GtExemplifierAdditions) for testing.
- Repo commit history will likely be modified to follow [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) to enable easy changelog generation and automatic version bumping.

## License
- AGPLv3+. See file LICENSE.
