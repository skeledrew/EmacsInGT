"
I am the root for ELisp objects.
"
Class {
	#name : #EigtObject,
	#superclass : #Object,
	#instVars : [
		'ast',
		'value',
		'meta',
		'quote',
		'context',
		'parent',
		'start',
		'stop',
		'source'
	],
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtObject class >> ast: anELispExpressionNode [
    "Initialize with the given AST node."
    | inst |
	inst := self new.
    inst ast: anELispExpressionNode.
    ^ inst.
]

{ #category : #accessing }
EigtObject class >> initWithAst: anAstNode andContext: anEigtContext andParent: anEigtObject andValue: anObject [
    "Primarily for initialization in the transpiler."
    ^ self new 
        context: anEigtContext;
        ast: anAstNode;
        value: anObject;
        parent: anEigtObject;
        yourself.
]

{ #category : #accessing }
EigtObject class >> initWithAst: anAstNode andContext: anEigtContext andValue: anObject [
    "Primarily for initialization in the transpiler."
    ^ self new 
        context: anEigtContext;
        ast: anAstNode;
        value: anObject;
        yourself.
]

{ #category : #accessing }
EigtObject class >> initWithContext: anEigtContext andValue: anObject [
    ""
    ^ self new 
        context: anEigtContext;
        value: anObject;
        parent: false;
        yourself.
]

{ #category : #accessing }
EigtObject class >> value: anObject [
    ^ self new value: anObject.
]

{ #category : #accessing }
EigtObject >> _eval [
    "Return self.
    
    NOTE: Self-evaluation is the default since only lists and symbols do anything else.
    "
    ^ self.
]

{ #category : #accessing }
EigtObject >> asContext [
    "Create an encapsulation that can be used for debugging."
    ^ EigtDebugContext from: self.
]

{ #category : #accessing }
EigtObject >> ast: anELispExpressionNode [
    "
    
    NOTE: This should be overridden by subclasses to properly collect the node value.
    "
    ast := anELispExpressionNode.
]

{ #category : #accessing }
EigtObject >> children [
    self subclassResponsibility.
]

{ #category : #accessing }
EigtObject >> context [
    context ifNil: [self error: 'Context not initialized'].
    ^ [EigtEngine getContextAt: context] on: KeyNotFound do: [:ex |
        (parent isKindOf: EigtObject) ifFalse: [ex signal].
        parent context.
    ].
]

{ #category : #accessing }
EigtObject >> context: anEigtContext [
    anEigtContext ifNil: [self error: 'Initializing context, but it is nil'].
    context := anEigtContext isNumber
        ifTrue: [anEigtContext]
        ifFalse: [anEigtContext identityHash].
]

{ #category : #accessing }
EigtObject >> error: aMessageString [
    ^ EigtError signal: aMessageString.
]

{ #category : #accessing }
EigtObject >> eval [
    "Return self.
    
    NOTE: Self-evaluation is the default since only lists and symbols do anything else.
    "
    Eigtils runBeforeEvalBlockIn: meta on: self.
    Eigtils runAfterEvalBlockIn: meta on: self.
    ^ self.
]

{ #category : #accessing }
EigtObject >> gtConnectionsFor: aView [
	<gtView>
	^ aView empty.
]

{ #category : #accessing }
EigtObject >> gtContextActionFor: anAction [
	<gtAction>
	^ anAction button
		icon: BrGlamorousVectorIcons search;
		priority: 13;
		action: [ :aButton | aButton phlow spawnObject: self context ];
		tooltip: 'View context'
]

{ #category : #accessing }
EigtObject >> gtEvalActionFor: anAction [
	<gtAction>
	^ anAction button
		icon: BrGlamorousVectorIcons play;
		priority: 11;
		action: [ :aButton | aButton phlow spawnObject: self eval ];
		tooltip: 'Evaluate'
]

{ #category : #accessing }
EigtObject >> gtParentActionFor: anAction [
	<gtAction>
	^ anAction button
		icon: BrGlamorousVectorIcons upwards;
		priority: 12;
		action: [ :aButton | aButton phlow spawnObject: (self parent ifNotNil: [aButton phlow spawnObject: self parent]) ];
		tooltip: (self parent ifNotNil: ['Go to parent'] ifNil: ['No parent found']).
]

{ #category : #accessing }
EigtObject >> gtPrintFor: aView [
	<gtView>
	^ aView empty.
]

{ #category : #accessing }
EigtObject >> gtSourceCodeFor: aView [
	<gtView>
	self source ifNil: [ ^ aView empty ].
	^ aView explicit
		title: 'Source code';
		priority: 5;
		actionButtonIcon: BrGlamorousVectorIcons playinspect
			tooltip: 'Evaluate'
			action: [:button | button phlow spawnObject: self eval ];
		actionButtonIcon: BrGlamorousVectorIcons transcript
			tooltip: 'Print'
			action: [:button | button phlow spawnObject: self eval print ];
		actionButtonIcon: BrGlamorousVectorIcons upwards
			tooltip: 'Parent'
			action: [:button | self parent ifNotNil: [button phlow spawnObject: self parent] ];
		stencil: [ | editor text |
			text := self source asRopedText.
			self stop > 1 ifTrue: [text
				attributes:
					{(BlTextUnderlineAttribute new
						color: BrGlamorousColors textHighlightColor;
						thickness: 3;
						beNotOverwritableByStyler)}
				from: self start
				to: self stop].
			editor := BrEditor new
				text: text;
				aptitude: BrGlamorousCodeEditorAptitude new;
				scrollToPosition: ( self source lineNumberCorrespondingToIndex: self start);
			    yourself. ]
]

{ #category : #accessing }
EigtObject >> initialize [
    meta := Dictionary new.
]

{ #category : #accessing }
EigtObject >> isEigtBoolVector [
    ^ self class = EigtBoolVector.
]

{ #category : #accessing }
EigtObject >> isEigtCharTable [
    ^ self class = EigtCharTable.
]

{ #category : #accessing }
EigtObject >> isEigtCharacter [
    ^ self class = EigtCharacter.
]

{ #category : #accessing }
EigtObject >> isEigtClosure [
    ""
    ^ self isEigtCons and: [(self value size >= 3) and: [value first class = EigtSymbol and: [value first symbolName = 'closure']]].
]

{ #category : #accessing }
EigtObject >> isEigtCons [
    ^ self class = EigtCons.
]

{ #category : #accessing }
EigtObject >> isEigtFloat [
    ^ self class = EigtFloat.
]

{ #category : #accessing }
EigtObject >> isEigtHashTable [
    ^ self class = EigtHashTable.
]

{ #category : #accessing }
EigtObject >> isEigtInteger [
    ^ self class = EigtInteger.
]

{ #category : #accessing }
EigtObject >> isEigtLambda [
    "NOTE: Just a cons with the proper signature."
    ^ self isEigtCons and: [(self value size >= 3) and: [value first class = EigtSymbol and: [value first symbolName = 'lambda']]].
]

{ #category : #accessing }
EigtObject >> isEigtMacro [
    "NOTE: Just a cons with the proper signature."
    ^ [
        (self isEigtCons & (value size = 2) and: [value first class = EigtSymbol and: [value first symbolName = 'macro']]) and: [value second isEigtLambda].
    ] on: Error do: [false].
]

{ #category : #accessing }
EigtObject >> isEigtRecord [
    ^ self class = EigtRecord.
]

{ #category : #accessing }
EigtObject >> isEigtString [
    ^ self class = EigtString.
]

{ #category : #accessing }
EigtObject >> isEigtSubr [
    "NOTE: A symbol with a function cell containing the nil symbol."
    ^ (self isEigtSymbol) and: [self symbolFunction class = EigtSubrGroup].
]

{ #category : #accessing }
EigtObject >> isEigtSymbol [
    ^ self class = EigtSymbol.
]

{ #category : #accessing }
EigtObject >> isEigtVector [
    ^ self class = EigtVector.
]

{ #category : #accessing }
EigtObject >> metaAt: aSymbol [
    ^ meta at: aSymbol.
]

{ #category : #accessing }
EigtObject >> metaAt: aSymbol put: anObject [
    ^ meta at: aSymbol put: anObject.
]

{ #category : #accessing }
EigtObject >> notImplemented [
    ^ self notYetImplemented
]

{ #category : #accessing }
EigtObject >> parent [
    ^ parent.
]

{ #category : #accessing }
EigtObject >> parent: anEigtObject [
    anEigtObject ifNil: [self error: 'Propagating a parent that is nil'].
    "anEigtObject = false ifTrue: [self record: self asString, ' setting parent to false']."
    parent := anEigtObject.
]

{ #category : #accessing }
EigtObject >> print [
    "Return the printable value."
    ^ value asString.
]

{ #category : #accessing }
EigtObject >> printOn: aStream [
	super printOn: aStream.
	aStream 
		nextPut: $<;
		nextPutAll: self print;
		nextPut: $>.
]

{ #category : #accessing }
EigtObject >> printType [
    ^ EigtSymbol new name: (self class name allButFirst: 4) translateToLowercase.
]

{ #category : #accessing }
EigtObject >> quote: aString [
    quote := aString.
]

{ #category : #accessing }
EigtObject >> record: anObject [
    "FIXME: We need to get the sender for indication purposes if this is to be useful."
    ^ EigtSignal emit: anObject.
]

{ #category : #accessing }
EigtObject >> source [
    source ifNotNil: [^ source].
    (parent isKindOf: EigtSequence) ifTrue: [^ parent source].
    parent = false ifTrue: [^ self print].
    self error: 'Unhandled case'.
]

{ #category : #accessing }
EigtObject >> specialFormP [
    "Ref: https://www.gnu.org/software/emacs/manual/html_node/elisp/Special-Forms.html"
    <eigtPrimitive: 'special-form-p'>
    (self symbolp = self context t and: [#('and' 'catch' 'cond' 'condition-case' 'defconst' 'defvar' 'function' 'if' 'interactive' 'lambda' 'let' 'let*' 'or' 'prog1' 'prog2' 'progn' 'quote' 'save-current-buffer' 'save-excursion' 'save-restriction' 'setq' 'setq-default' 'unwind-protect' 'while') 
        contains: [:sfs | sfs = self symbolName]]) ifTrue: [^ self context t].
    ^ self context nil.
]

{ #category : #accessing }
EigtObject >> start [
    self subclassResponsibility.
]

{ #category : #accessing }
EigtObject >> start: anInteger [
    start := anInteger.
]

{ #category : #accessing }
EigtObject >> stop [
    self subclassResponsibility.
]

{ #category : #accessing }
EigtObject >> stop: anInteger [
    stop := anInteger.
]

{ #category : #accessing }
EigtObject >> symbolp [
    ^ ((self isMemberOf: EigtSymbol)) ifTrue: [self context t] ifFalse: [self context nil].
]

{ #category : #accessing }
EigtObject >> value [
    ^ value.
]

{ #category : #accessing }
EigtObject >> value: anObject [
    self subclassResponsibility.
]
