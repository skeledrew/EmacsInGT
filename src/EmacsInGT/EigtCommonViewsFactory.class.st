Class {
	#name : #EigtCommonViewsFactory,
	#superclass : #Object,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtCommonViewsFactory class >> gtAtomicPreviewFor: aView withTitle: aNameString andData: aDataString [
	^ aView explicit
		title: aNameString;
		stencil: [ 
			| aNumberElement |
			aNumberElement := BrLabel new 
				text: aDataString;
				aptitude: BrGlamorousLabelAptitude + BrShadowAptitude;
				background: Color white;
				margin: (BlInsets all: 20);
				padding: (BlInsets all: 10);
				layout: BlLinearLayout horizontal;
				constraintsDo: [ :c | 
					c vertical fitContent.
					c horizontal fitContent.
					c frame horizontal alignCenter.
					c frame vertical alignCenter ].
			BlElement new 
				constraintsDo: [:c | 
					c vertical matchParent.
					c horizontal matchParent];
				layout: BlFrameLayout new;
				addChild: aNumberElement ]
]
