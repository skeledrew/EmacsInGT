Extension { #name : #AbstractFileReference }

{ #category : #'*EmacsInGT' }
AbstractFileReference >> gtELispFor: aView [
	<gtView>
	| engine contentsIn |
	self isFile & self extension = 'el' ifFalse: [ ^ aView empty ].
	engine := EigtEngine new.
	contentsIn := [:tab | tab viewContentElement children first editor text asString].
	^ aView explicit
		title: 'ELisp';
		priority: 5;
		stencil: [ 
			BrEditorElement new 
				padding: (BlInsets all: 10);
				constraintsDo: [ :c | 
					c horizontal matchParent.
					c vertical matchParent ];
				editor: (BrTextEditor new 
					text: (BlRopedText file: self));
					aptitude: BrGlamorousCodeEditorAptitude.
		  ];
		actionStencil: [:tab |
			BrButton new 
				aptitude: BrGlamorousButtonWithIconAptitude;
				label: 'Read';
				icon: BrGlamorousVectorIcons inspect;
				action: [ :button |
					button phlow spawnObject: (engine read: (contentsIn value: tab)) ]];
		actionStencil: [:tab |
		    BrButton new 
				aptitude: BrGlamorousButtonWithIconAptitude;
				label: 'Evaluate';
				icon: BrGlamorousVectorIcons playinspect;
				action: [ :button |
					button phlow spawnObject:
					    (engine read: (contentsIn value: tab); eval). ]];
		actionStencil: [:tab |
			BrButton new 
				aptitude: BrGlamorousButtonWithIconAptitude;
				label: 'Print';
				icon: BrGlamorousVectorIcons transcript;
				action: [ :button |
					button phlow spawnObject:
					    (engine read: (contentsIn value: tab); eval; print). ]];
		actionStencil: [ :tab | 
			BrButton new 
				aptitude: BrGlamorousButtonWithIconAptitude;
				label: 'Save';
				icon: BrGlamorousVectorIcons accept;
				action: [ 
					| newContents | 
					newContents := tab viewContentElement children first editor text asString.
					(self fullName, '.backup') asFileReference ensureDelete.
					self copyTo: (self fullName, '.backup') asFileReference.
					self ensureDelete; writeStreamDo: [ :s | s nextPutAll: newContents ] ] ]
]
