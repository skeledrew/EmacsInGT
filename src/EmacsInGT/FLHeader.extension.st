Extension { #name : #FLHeader }

{ #category : #'*EmacsInGT' }
FLHeader >> eigtGetMetaData [
    "Read the header for transferal."
    | meta |
	meta := Dictionary new.
	meta at: #extra put: additionalObjects.
	meta at: #pre put: preMaterializationActions.
	meta at: #post put: postMaterializationActions.
	^ meta.
]

{ #category : #'*EmacsInGT' }
FLHeader >> eigtSetMetaData: aDictionary [
    "Write new data to header."
    additionalObjects := aDictionary at: #extra.
    preMaterializationActions := aDictionary at: #pre.
    postMaterializationActions := aDictionary at: #post.
]
