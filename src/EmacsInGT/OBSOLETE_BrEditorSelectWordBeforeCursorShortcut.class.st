Class {
	#name : #'OBSOLETE_BrEditorSelectWordBeforeCursorShortcut',
	#superclass : #BrEditorShortcut,
	#category : #EmacsInGT
}

{ #category : #'api - combination' }
OBSOLETE_BrEditorSelectWordBeforeCursorShortcut >> combinationForMacOS [
	^ combinationForMacOS
]

{ #category : #accessing }
OBSOLETE_BrEditorSelectWordBeforeCursorShortcut >> description [
	^ 'Selects one word at a time to the left from the cursor.'
]

{ #category : #accessing }
OBSOLETE_BrEditorSelectWordBeforeCursorShortcut >> initialize [
	super initialize.
	
	combination := BlKeyCombination shiftPrimaryArrowLeft.
]

{ #category : #accessing }
OBSOLETE_BrEditorSelectWordBeforeCursorShortcut >> name [
	^ 'Select word to the left'
]

{ #category : #accessing }
OBSOLETE_BrEditorSelectWordBeforeCursorShortcut >> performOnEditor: aBrTextEditor element: aBrEditorElement dueTo: aShortcutEvent [
    | selecter startPos text endPos curPos hitNonWS |
    selecter := aBrTextEditor selecter.
    startPos := selecter cursors first position. "assume a single cursor"
    startPos = 0 ifTrue: [^ self].
    text := selecter editor text asString.
    endPos := DEPRECATED_ShortcutFactory findEndPosIn: text for: '\s' startingAt: startPos going: #left.
    endPos = nil ifTrue: [^ self].
    selecter
		withoutCursorUpdate;
		moveBy: endPos - startPos;
		select.
	aBrTextEditor navigator
		moveToStartPreviousWord;
		apply
]
