Class {
	#name : #EigtCons,
	#superclass : #EigtSequence,
	#instVars : [
		'inQuote',
		'car',
		'cdr',
		'lexicalNS'
	],
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtCons class >> empty [
    ^ self nil.
]

{ #category : #accessing }
EigtCons class >> nil [
    ^ self new nil.
]

{ #category : #accessing }
EigtCons class >> nilWithContext: anEigtContext [
    ^ EigtCons new
        context: anEigtContext;
        empty;
        yourself.
]

{ #category : #accessing }
EigtCons class >> withContext: anEigtContextHolder andValue: aCollection [
    "Create an instance and initialize it."
    ^ self new
        context: (anEigtContextHolder isNumber
            ifTrue: [anEigtContextHolder]
            ifFalse: [anEigtContextHolder context]);
        parent: false;
        value: aCollection;
        yourself.
]

{ #category : #accessing }
EigtCons >> _eval [
    "Evaluate self value.
    
    TODO: revise indirection mutating the cons
    "
    | size result unwrapped args functionSource formType tCar originalCar |
    parent ifNil: [self error: 'The parent was not initialized'].
    "EigtSignal emit: 'Enter eval of: ', self print, '; cid: ', context asString."
    self context increaseCallDepth.
    self context lexicalBinding ifTrue: [self initLexicalNS].
    value size = 0 ifTrue: [^ self].
    originalCar := tCar := value first.
    tCar context ifNil: [tCar context: context].
    tCar := self maybePatchValueCarGiven: tCar.
    (self shouldEvalAsMacroGiven: tCar) ifTrue: [^ self evalMacro].
    (self shouldEvalAsSpecialFormGiven: tCar) ifTrue: [^ self evalSpecialForm].
    (self shouldEvalAsFunctionGiven: tCar) ifTrue: [^ self evalFunction].
    value at: 1 put: originalCar.
    result ifNil: [self error: 'Evaluation failed'].
    ^ result.
]

{ #category : #accessing }
EigtCons >> ast: anELispExpressionNode [
    super ast: anELispExpressionNode.
]

{ #category : #accessing }
EigtCons >> car [
    ""
    ^ car.
]

{ #category : #accessing }
EigtCons >> cdr [
    ""
    ^ cdr.
]

{ #category : #accessing }
EigtCons >> cons: anEigtObject to: anotherEigtObject [
    ""
    "context := anEigtObject context.
    context ifNil: [self error: 'Context was not initialized']."
    car := anEigtObject.
    cdr := anotherEigtObject.
    value := {anEigtObject. anotherEigtObject}.
    self setChildrenParentsAndStarts.
]

{ #category : #accessing }
EigtCons >> empty [
    ^ self nil.
]

{ #category : #accessing }
EigtCons >> eval [
    | result |
	Eigtils runBeforeEvalBlockIn: meta on: self.
    result := self _eval.
    Eigtils runAfterEvalBlockIn: meta on: self.
    ^ result.
]

{ #category : #accessing }
EigtCons >> evalFunction [
    "Evaluate a function form."
    | args function symName spacer subr result |
	args := self value allButFirst collect: [:el | (el context: context; eval) context: context].
	function := value first.
	function context: context.
    function isEigtSubr ifTrue: [
        "Primitive call."
        symName := function print.
        subr := self context findSubr: symName.
        subr ifNotNil: [
            (subr methodClass instanceSide respondsTo: subr selector) ifFalse: [self error: 'Need to set `', subr methodClass instanceSide name, '>>#', subr selector, '` to class instead of instance'].
            result := subr methodClass instanceSide
                perform: subr selector 
                withArguments: {EigtArgsList new put: args; context: context; yourself}
        ] ifNil: [
            spacer := value size > 1 ifTrue: [' '''] ifFalse: [''].
            result := (self context readWithEngine: (self context evalOnServer: '(', symName, spacer, ((' ''' join: (args collect: [:el | el print]))), ')')) value first.]
    ].
    (function isEigtLambda or: [function isEigtClosure]) ifTrue: [
        "Lambda/closure call"
        result := EigtSubrGroup call: 'funcall' with: 
            (EigtArgsList from: (args addFirst: function; yourself) context: context).
    ].
    self context decreaseCallDepth.
    "EigtSignal emit: 'Exit eval of: ', self print."
    ^ result.
]

{ #category : #accessing }
EigtCons >> evalMacro [
	""

	| operator args result |
	args := OrderedCollection new
		add: car symbolFunction cdr;
		"NOTE: Assumes a symbol containing the function."
			addAll: self value allButFirst;
		yourself.
	result := EigtSubrGroup
		call: 'funcall'
		with: (EigtArgsList from: args context: context).
	result parent: false.
	result := result eval.  "TODO: Confirm if we should ectually eval the expansion here"
	self context decreaseCallDepth.
	"EigtSignal emit: 'Exit eval of: ', self print."
	^ result.
]

{ #category : #accessing }
EigtCons >> evalSpecialForm [
    ""
    | aName result subr |
	aName := value first symbolName.
    subr := (self context findSubr: aName) ifNil: [self error: 'Unable to find subr: ', aName].
    (subr methodClass instanceSide respondsTo: subr selector) ifFalse: [self error: 'Need to set `', subr methodClass instanceSide name, '>>#', subr selector, '` to class instead of instance'].
    result := subr methodClass instanceSide
        perform: subr selector  "(aName, ':') asSymbol "
        withArguments: {EigtArgsList new put: self value allButFirst; context: context; yourself}.
    self context decreaseCallDepth.
    "EigtSignal emit: 'Exit eval of: ', self print, '; cid: ', context value identityHash asString."
    ^ result.
]

{ #category : #accessing }
EigtCons >> evalWith: anEigtContext [
    context := anEigtContext.
    ^ self eval.
]

{ #category : #accessing }
EigtCons >> initLexicalNS [
    "Initialize the lexical namespace if this cons is valid for binding."
    lexicalNS ifNotNil: [^ self].
    "EigtSignal emit: self."
    (car isEigtSymbol and: [cdr isEigtCons or: [cdr = self context nil]]) ifFalse: [^ self].
    (#(let #'let*' closure) anySatisfy: [:sym | car symbolName asSymbol = sym]) ifFalse: [^ self].
    lexicalNS := EigtNamespace withContext: context.
]

{ #category : #accessing }
EigtCons >> initialize [
    super initialize.
    firstChildPos := 2.
]

{ #category : #accessing }
EigtCons >> lexicalNS [
    ^ lexicalNS.
]

{ #category : #accessing }
EigtCons >> maybePatchValueCarGiven: anEigtObject [
    "Handle indirection."
    | tCar |
	tCar := anEigtObject.
    [(tCar isEigtSymbol) and: [(tCar symbolFunction class = EigtSymbol) | ([tCar symbolFunction isEigtLambda] on: Error do: [false]) | ([tCar symbolFunction isEigtClosure] on: Error do: [false])]]
        whileTrue: [value at: 1 put: (tCar := tCar symbolFunction)].
    ^ tCar.
]

{ #category : #accessing }
EigtCons >> nil [
    car := cdr := false.
    parent := false.
]

{ #category : #accessing }
EigtCons >> print [
    ""
    (car = false) & (cdr = false) ifTrue: [^ 'nil'].
    (car = self context nil) & (cdr = self context nil) ifTrue: [^ '()'].
    (car = nil) | (cdr = nil) ifTrue: [^ '!ERROR!'].
    ^ (cdr class = EigtCons or: cdr = self context nil)
        ifTrue: ['(', (' ' join: (self value collect: [:el | el print])), ')']
        ifFalse: [
            '(', car print, ' . ', cdr print, ')'.
        ].
]

{ #category : #accessing }
EigtCons >> shouldEvalAsFunctionGiven: tCar [
    ^ (tCar isEigtLambda or: [tCar isEigtClosure or: [tCar isEigtSubr]]) & (tCar specialFormP = self context nil).
]

{ #category : #accessing }
EigtCons >> shouldEvalAsMacroGiven: tCar [
    ^ tCar isEigtSymbol and: [[tCar symbolFunction isEigtMacro] on: MessageNotUnderstood do: [false]]
]

{ #category : #accessing }
EigtCons >> shouldEvalAsSpecialFormGiven: tCar [
    ^ tCar specialFormP = self context t.
]

{ #category : #accessing }
EigtCons >> value [
    "Gather cons'd objects into a collection and return it.
    
    NOTE: Since value is created from a collection without a terminating `nil`, it is undefined for pairs which have no equivalency as a collection target.
    "
    | coll curCons |
    (car = false) & (cdr = false) ifTrue: [^ OrderedCollection new].
    value ifNil: [self error: 'Uninitialized cons object'].
    (cdr isMemberOf: EigtCons) not & (cdr ~= self context nil)
        ifTrue: [self error: 'This is a cons pair; value is undefined'].
    coll := OrderedCollection new.
    curCons := self.
    [curCons cdr = self context nil] whileFalse: [
        "NOTE: Termination depends on `context nil` erroring on access."
        coll add: curCons car.
        curCons := curCons cdr.
    ].
    coll add: curCons car.
    ^ coll.
]

{ #category : #accessing }
EigtCons >> value: aCollection [
    "Store a collection as a nested cons."
    context ifNil: [self error: 'Context was not initialized'].
    ((aCollection isKindOf: Collection) 
        and: [aCollection allSatisfy: [:ele | ele isKindOf: EigtObject]])
            ifFalse: [self error:'A collection of EigtObject is expected'].
    value isNil
        ifTrue: [
            car := aCollection size isZero ifTrue: [self context nil] ifFalse: [aCollection first parent: self; yourself]. 
            cdr := aCollection size <= 1 
                ifTrue: [self context nil] "TODO: This should be an empty cons maybe"
                ifFalse: [EigtCons new
                    context: context;
                    parent: self;
                    start: self start;
                    stop: self stop;
                    value: aCollection allButFirst.
                ].
            value := {car . cdr}.
        ]
        ifFalse: [self error: 'Value is write once.'].
    (cdr isMemberOf: EigtCons) | (cdr = self context nil) ifFalse: [self error: 'Expected last item to be a cons or nil'].
    self setChildrenParentsAndStarts.
]
