Class {
	#name : #EigtSignal,
	#superclass : #ContextStackSignal,
	#instVars : [
		'extra'
	],
	#category : #'EmacsInGT-Utilities'
}

{ #category : #accessing }
EigtSignal class >> emit: anObject [
    ^ self new emit: anObject.
]

{ #category : #accessing }
EigtSignal >> emit: anObject [
    extra := anObject.
    super emit.
]

{ #category : #accessing }
EigtSignal >> extra [
    ^ extra.
]

{ #category : #accessing }
EigtSignal >> printOn: stream [
	super printOn: stream.
	self stack do: [ :each |
		stream 
			cr;
			tab; 
			nextPutAll: (
				each asString 
					copyReplaceAll: String cr 
					with: String cr, 
							String tab, 
							String tab) ].
	stream nextPutAll: extra asString.
]

{ #category : #accessing }
EigtSignal >> printOneLineContentsOn: stream [
	stream nextPutAll: (self stack first asString 
					copyReplaceAll: String cr 
					with: String cr, 
							String tab, 
							String tab).
	stream nextPutAll: ' :: ', self extra asString.
]
