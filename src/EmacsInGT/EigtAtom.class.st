"
ELisp atom representation.
"
Class {
	#name : #EigtAtom,
	#superclass : #EigtObject,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtAtom >> = anObject [
    ^ self class = anObject class and: [value = anObject value].
]

{ #category : #accessing }
EigtAtom >> _gtPreviewFor: aView withTitle: aString [
	^ aView explicit
		title: aString;
		stencil: [ 
			| aNumberElement |
			aNumberElement := BrLabel new 
				text: self print;
				aptitude: BrGlamorousLabelAptitude + BrShadowAptitude;
				background: Color white;
				margin: (BlInsets all: 20);
				padding: (BlInsets all: 10);
				layout: BlLinearLayout horizontal;
				constraintsDo: [ :c | 
					c vertical fitContent.
					c horizontal fitContent.
					c frame horizontal alignCenter.
					c frame vertical alignCenter ].
			BlElement new 
				constraintsDo: [:c | 
					c vertical matchParent.
					c horizontal matchParent];
				layout: BlFrameLayout new;
				addChild: aNumberElement ]
]

{ #category : #accessing }
EigtAtom >> children [
    ^ #().
]

{ #category : #accessing }
EigtAtom >> gtPreviewFor: aView [
    <gtView>
    ^ self _gtPreviewFor: aView withTitle: 'Preview'
]

{ #category : #accessing }
EigtAtom >> start [
    ^ parent ifNil: [1] ifNotNil: [start].
]

{ #category : #accessing }
EigtAtom >> stop [
    ^ parent ifNil: [self print size] ifNotNil: [start + self print size - 1].
]

{ #category : #accessing }
EigtAtom >> value: anObject [
    "Store a value who's type is representative of the object's class."
    value isNil
        ifTrue: [value := anObject]
        ifFalse: [self error: 'Value is write once.'].
]
