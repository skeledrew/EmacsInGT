Class {
	#name : #EigtUnquote,
	#superclass : #EigtCons,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtUnquote >> children [
    ^ {self value}.
]

{ #category : #accessing }
EigtUnquote >> eval [
    | result |
	result := value eval.
	quote = ',@' & (self context peekQuote = '`') ifTrue: [result := result value].  "trigger splice"
	^ result.
]

{ #category : #accessing }
EigtUnquote >> initialize [
    super initialize.
    quote := ','.
]

{ #category : #accessing }
EigtUnquote >> print [
    ^ quote, value print.
]

{ #category : #accessing }
EigtUnquote >> value [
    ^ value.
]

{ #category : #accessing }
EigtUnquote >> value: anEigtObject [
    value := anEigtObject.
    firstChildPos := quote size + 1.
]
