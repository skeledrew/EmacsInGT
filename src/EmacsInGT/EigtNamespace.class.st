"
TODO: Convert to composed type under EigtObject.
"
Class {
	#name : #EigtNamespace,
	#superclass : #Dictionary,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtNamespace class >> new [
    ^ super new initialize; yourself.
]

{ #category : #accessing }
EigtNamespace class >> withContext: anEigtContext [
    | inst |
	inst := self new.
    (inst at: #t) context: anEigtContext; name: #t.
    (inst at: #nil) context: anEigtContext; name: #nil.
    ^ inst.
]

{ #category : #accessing }
EigtNamespace >> initialize [
    ""
    self at: #t put: EigtSymbol new.
    self at: #nil put: EigtSymbol new.
]
