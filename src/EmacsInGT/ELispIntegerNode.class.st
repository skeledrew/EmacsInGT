Class {
	#name : #ELispIntegerNode,
	#superclass : #ELispExpressionNode,
	#instVars : [
		'value'
	],
	#category : #'EmacsInGT-AST'
}

{ #category : #generated }
ELispIntegerNode >> acceptVisitor: anExpressionVisitor [
	^ anExpressionVisitor visitInteger: self
]

{ #category : #generated }
ELispIntegerNode >> tokenVariables [
	^ #(#value)
]

{ #category : #generated }
ELispIntegerNode >> value [
	^ value
]

{ #category : #generated }
ELispIntegerNode >> value: aSmaCCToken [
	value := aSmaCCToken
]
