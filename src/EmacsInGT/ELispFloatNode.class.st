Class {
	#name : #ELispFloatNode,
	#superclass : #ELispExpressionNode,
	#instVars : [
		'value'
	],
	#category : #'EmacsInGT-AST'
}

{ #category : #generated }
ELispFloatNode >> acceptVisitor: anExpressionVisitor [
	^ anExpressionVisitor visitFloat: self
]

{ #category : #generated }
ELispFloatNode >> tokenVariables [
	^ #(#value)
]

{ #category : #generated }
ELispFloatNode >> value [
	^ value
]

{ #category : #generated }
ELispFloatNode >> value: aSmaCCToken [
	value := aSmaCCToken
]
