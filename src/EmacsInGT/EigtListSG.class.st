Class {
	#name : #EigtListSG,
	#superclass : #EigtSubrGroup,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtListSG class >> assq: anEigtArgsList [
    <eigtSubr: 'assq'>
    anEigtArgsList second value do: [:assoc |
        (assoc isEigtCons and: [assoc car = anEigtArgsList first])
            ifTrue: [^ assoc]
    ].
    ^ anEigtArgsList context nil.
]

{ #category : #accessing }
EigtListSG class >> car: anEigtArgsList [
    <eigtSubr: 'car'>
    anEigtArgsList get size = 1 ifFalse: [self error: 'Wrong number of arguments: car, ', anEigtArgsList get size asString].
    anEigtArgsList first class = EigtCons ifFalse: [self error: 'Wrong type argument: listp ', anEigtArgsList first print].
    ^ anEigtArgsList first car.
]

{ #category : #accessing }
EigtListSG class >> carSafe: anEigtArgsList [
    <eigtSubr: 'car-safe'>
    ^ anEigtArgsList first class = EigtCons
        ifTrue: [self car: anEigtArgsList]
        ifFalse: [anEigtArgsList context nil].
]

{ #category : #accessing }
EigtListSG class >> cdr: anEigtArgsList [
    <eigtSubr: 'cdr'>
    anEigtArgsList get size = 1 ifFalse: [self error: 'Wrong number of arguments: car, ', anEigtArgsList get size asString].
    anEigtArgsList first class = EigtCons ifFalse: [self error: 'Wrong type argument: listp ', anEigtArgsList first print].
    ^ anEigtArgsList first cdr.
]

{ #category : #accessing }
EigtListSG class >> cdrSafe: anEigtArgsList [
    <eigtSubr: 'cdr-safe'>
    ^ anEigtArgsList first class = EigtCons
        ifTrue: [self cdr: anEigtArgsList]
        ifFalse: [anEigtArgsList context nil].
]

{ #category : #accessing }
EigtListSG class >> cons: anEigtArgsList [
    <eigtSubr: 'cons'>
    ^ EigtCons new
        context: anEigtArgsList context;
        cons: anEigtArgsList get first to: anEigtArgsList get second;
        parent: false;
        yourself.
]

{ #category : #accessing }
EigtListSG class >> consP: anEigtArgsList [
    <eigtSubr: 'consp'>
    ^ anEigtArgsList first class = EigtCons
        ifTrue: [anEigtArgsList context t]
        ifFalse: [anEigtArgsList context nil].
]

{ #category : #accessing }
EigtListSG class >> list: anEigtArgsList [
    <eigtSubr: 'list'>
    ^ EigtCons new
        context: anEigtArgsList context;
        value: anEigtArgsList get;
        parent: false;
        yourself.
]

{ #category : #accessing }
EigtListSG class >> listP: anEigtArgsList [
    <eigtSubr: 'listp'>
    ^ (anEigtArgsList first class = EigtCons) | (anEigtArgsList first = anEigtArgsList context nil)
        ifTrue: [anEigtArgsList context t]
        ifFalse: [anEigtArgsList context nil].
]

{ #category : #accessing }
EigtListSG class >> mapcar: anEigtArgsList [
    "TODO: Handle vector and string (pg 251)"
    <eigtSubr: 'mapcar'>
    | results applyOp |
    "applyOp := anEigtArgsList context findSubr: #apply."
    anEigtArgsList get second = anEigtArgsList context nil
        ifTrue: [^ anEigtArgsList context nil].
    results := anEigtArgsList get second value
	    collect: [:eo | 
	        self
	            call: 'funcall'
	            with: (EigtArgsList 
	                withContext: anEigtArgsList
	                andPut: {anEigtArgsList first . eo})
	    ].
    ^ EigtCons withContext: anEigtArgsList andValue: results.
]
