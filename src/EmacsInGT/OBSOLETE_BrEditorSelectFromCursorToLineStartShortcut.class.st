Class {
	#name : #'OBSOLETE_BrEditorSelectFromCursorToLineStartShortcut',
	#superclass : #BrEditorShortcut,
	#category : #EmacsInGT
}

{ #category : #accessing }
OBSOLETE_BrEditorSelectFromCursorToLineStartShortcut >> description [
	^ 'Selects from the cursor to the start of the line.'
]

{ #category : #accessing }
OBSOLETE_BrEditorSelectFromCursorToLineStartShortcut >> initialize [
	super initialize.
	
	combination := BlKeyCombination shiftHome.
]

{ #category : #accessing }
OBSOLETE_BrEditorSelectFromCursorToLineStartShortcut >> name [
	^ 'Select to line start'
]

{ #category : #accessing }
OBSOLETE_BrEditorSelectFromCursorToLineStartShortcut >> performOnEditor: aBrTextEditor element: aBrEditorElement dueTo: aShortcutEvent [
    | selecter startPos text endPos curPos hitNonWS |
    selecter := aBrTextEditor selecter.
    startPos := selecter cursors first position max: 1. "assume a single cursor"
    text := selecter editor text asString.
    startPos >= text size ifTrue: [^ self].
    endPos := DEPRECATED_ShortcutFactory findEndPosIn: text for: '\n|\r' replaceEscapeSequences startingAt: startPos going: #left.
    selecter
		withoutCursorUpdate;
		moveBy: endPos - startPos;
	    select.
	aBrTextEditor navigator
		moveToLineStart;
		apply
]
