"
I provide the primary entrypoint into using Emacs in GToolkit.

NOTE (21-06-01): `context` was made to be the identityHash of the actual context to reduce the number of changes required, and to ease the route to undoing the changes once the cause of multiple contexts is found.
"
Class {
	#name : #EigtEngine,
	#superclass : #Object,
	#instVars : [
		'globalNS',
		'inQuote',
		'cachedExpr',
		'cachedResult',
		'context',
		'transpiler',
		'readAnnouncer',
		'cachedSrc'
	],
	#classInstVars : [
		'allContexts'
	],
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtEngine class >> getContextAt: anInteger [
    | context |
    anInteger class = EigtContext ifTrue: [
        EigtSignal emit: 'Context instead of context id passed'.
        allContexts at: anInteger identityHash. "TODO: Remove after some testing."
        ^ anInteger.
    ].
	context := allContexts at: anInteger.
	context identityHash = anInteger ifFalse: [self error: 'Invalid context hash!!!'].
	^ context.
]

{ #category : #accessing }
EigtEngine class >> registerContext: anEigtContext [
    allContexts ifNil: [allContexts := Dictionary new].
    allContexts at: anEigtContext identityHash put: anEigtContext.
    ^ anEigtContext identityHash.
]

{ #category : #accessing }
EigtEngine >> context [
    ^ self class getContextAt: context.
]

{ #category : #accessing }
EigtEngine >> createFuelFrom: aFile [
    | obj fuelPath |
    fuelPath := (aFile pathString, '.fuel').
	obj := self read: aFile contents.
	obj source: aFile contents.
	Eigtils save: obj toFuelFile: fuelPath.
	Eigtils inFuelFile: fuelPath atHeaderEntry: #crc16 put: (CRC crc16FromCollection: aFile contents).
	^ obj.
]

{ #category : #accessing }
EigtEngine >> eval [
    "Evaluate a cached EigtObject.
    
    NOTE: This is a convenience method to enable chaining from #read:.
    "
    cachedResult := self eval: nil.
	^ cachedResult.
]

{ #category : #accessing }
EigtEngine >> eval: anEigtObject [
    "Evaluate anEigtObject and return the result."
    | elisp |
	anEigtObject isNil
        ifTrue: [
            cachedExpr isNil ifTrue: [self error: 'No expression node provided'].
        elisp := cachedExpr.
        ]
        ifFalse: [elisp := anEigtObject].
    elisp
        context: context.
    cachedResult := elisp eval.
    cachedResult ifNil: [^ EigtVoid new].
    [cachedResult context] on: EigtError do: [^ cachedResult].  "FIXME: Hack to get around numbers (and maybe other atoms) missing context"
    ^ cachedResult.
]

{ #category : #accessing }
EigtEngine >> gtConnectionsFor: aView [
    <gtView>
    ^ aView empty.
]

{ #category : #accessing }
EigtEngine >> gtContextActionFor: anAction [
	<gtAction>
	^ anAction button
		icon: BrGlamorousVectorIcons search;
		priority: 10;
		action: [ :aButton | aButton phlow spawnObject: self context ];
		tooltip: 'View context'
]

{ #category : #accessing }
EigtEngine >> gtEditorFor: aView [
    <gtView>
    | frame mainWindow miniEditor mainEditor anAnnouncer |
    anAnnouncer := Announcer new.
	frame := BlElement new 
        layout: BlLinearLayout vertical;
        constraintsDo: [:c | c vertical matchParent. c horizontal matchParent].
    mainEditor := BrEditor new.
    mainWindow := mainEditor
        text: cachedSrc;
        constraintsDo: [:c | c vertical matchParent. c horizontal matchParent].
    "mainWindow addChild: (BrResizer new
        beBottom;
        target: mainWindow;
        aptitude: BrGlamorousResizerAptitude;
        elevation: (BlRelativeElevation elevation: 1000);
        constraintsDo: [ :c | c ignoreByLayout ])."
    frame addChild: mainWindow.
    miniEditor := BrEditor new.
    frame addChild: (miniEditor
        text: '';
        constraintsDo: [:c | c vertical matchParent. c horizontal matchParent]).
    ^ aView explicit
        title: 'Editor';
        priority: 11;
        updateWhen: Announcement in: [anAnnouncer];
        stencil: [frame];
        actionButtonIcon: BrGlamorousVectorIcons inspect
            tooltip: 'Read'
            action: [:button |
                | contents |
			    contents := mainEditor dataSource text asString.
			    self read: contents.
			    button phlow spawnObject: cachedExpr.
        ];
        actionButtonIcon: BrGlamorousVectorIcons playinspect
            tooltip: 'Evaluate'
            action: [:button |
                | contents |
			    contents := mainEditor dataSource text asString.
			    self read: contents; eval.
			    button phlow spawnObject: cachedResult.
        ];
        actionButtonIcon: BrGlamorousVectorIcons transcript
            tooltip: 'Print'
            action: [:button |
                | contents |
			    contents := mainEditor dataSource text asString.
			    self read: contents; eval.
			    button phlow spawnObject: cachedResult print.
        ].
]

{ #category : #accessing }
EigtEngine >> gtEvalFor: aView [
    "<gtView>"
    "(cachedResult isKindOf: EigtObject) ifFalse: [^ aView empty]."
    ^ aView explicit
        title: 'Eval';
        priority: 13;
        stencil: [
            ((cachedResult isKindOf: EigtAtom)
                ifTrue: [cachedResult gtPreviewFor: aView]
                ifFalse: [cachedResult gtObjectsFor: aView]) asElement
        ]
]

{ #category : #accessing }
EigtEngine >> gtPrintFor: aView [
    <gtView>
    ^ aView empty.
]

{ #category : #accessing }
EigtEngine >> gtReadFor: aView [
    "<gtView>"
    "(cachedExpr isKindOf: EigtObject) ifFalse: [^ aView empty]."
    ^ aView explicit
        title: 'Read';
        priority: 12;
        stencil: [
            ((cachedExpr isKindOf: EigtAtom)
                ifTrue: [cachedExpr gtPreviewFor: aView]
                ifFalse: [cachedExpr gtObjectsFor: aView]) asElement
        ];
        updateWhen: Announcement in: [readAnnouncer].
]

{ #category : #accessing }
EigtEngine >> initBuiltins [
    "Initialize built-in variables."
    | initScript |
    initScript := '(defconst lexical-binding nil)'.
	self
	    read: initScript;
	    eval.
]

{ #category : #accessing }
EigtEngine >> initObjectRecursively: anEigtObject [
    anEigtObject deep: #children do: [:child |
	        child context: context.
	        child class = EigtSymbol ifTrue: [
	            (self class getContextAt: context)
	                inNamespaceAt: child symbolName asSymbol put: child
	        ].
	        (child isKindOf: EigtSequence) ifTrue: [
	            child setChildrenParentsAndStarts
	        ]
	    ]. 
]

{ #category : #accessing }
EigtEngine >> initialize [
    context := self class registerContext: EigtContext new.
    transpiler := EigtTranspiler new.
    transpiler context: context.
    (self class getContextAt: context) engine: self.
    self initBuiltins.
    "self loadLisp."
    cachedResult := nil.
    readAnnouncer := Announcer new.
    cachedSrc := ''.
]

{ #category : #accessing }
EigtEngine >> loadLisp [
    "TODO: Eval multiple files in proper order."
    | lispDirs elFile |
	lispDirs := Eigtils loadSimpleKVData at: #lispDirs.
    cachedExpr := self readPath: lispDirs.
    ^ self eval.
]

{ #category : #accessing }
EigtEngine >> print [
    ^ cachedResult print
]

{ #category : #accessing }
EigtEngine >> read: aString [
    "Read aString and convert to an ELispObject."
    | node |
    context ifNil: [self error: 'Failed to create context'].
    cachedSrc := aString.
    node := ELispParser parse: aString.
    cachedExpr := transpiler acceptNode: node. "NOTE: always an EigtFile"
    cachedExpr
        source: aString.
    cachedResult := nil.
    ^ cachedExpr.
]

{ #category : #accessing }
EigtEngine >> readAndCreateFuelFrom: aFile [
    | obj fuelPath fObj |
    fuelPath := (aFile pathString, '.fuel').
	obj := self read: aFile contents.
	"Halt now."
	obj source: aFile contents.
	Eigtils save: obj toFuelFile: fuelPath.
	Eigtils inFuelFile: fuelPath atHeaderEntry: #crc16 put: (CRC crc16FromCollection: aFile contents).
	"Eigtils inFuelFile: fuelPath atHeaderEntry: #globals put: obj context copyGlobals.
	Eigtils inFuelFile: fuelPath atHeaderEntry: #references put: obj context refs."
	fObj := Eigtils loadFuelFile: fuelPath.  "NOTE: Reload the object to ensure it was properly saved and consistency sanity check"
	(obj = fObj)
	    ifFalse: [self error: 'Fuel-persisted object is broken'].
	^ fObj.
]

{ #category : #accessing }
EigtEngine >> readFileOrFolder: aFileReference [
    ^ Eigtils cond: {
	    {[aFileReference isFile]. [{aFileReference}]}.
	    {
	        [aFileReference isDirectory]. 
	        [aFileReference allFiles select: [:file | file fullName endsWith: '.el']]
	    }.
        {true. [self error: 'Invalid file type.']}
    }.
]

{ #category : #accessing }
EigtEngine >> readPath: aStringOrCollection [
    "Read ELisp file(s) or all ELisp files recursively if given folders."
    | elFiles elFileContents path nl sep fileCache fileColl |
	elFiles := OrderedCollection new.
	elFileContents := OrderedCollection new.
	fileCache := OrderedCollection new.
	fileColl:= EigtFile new.
	Eigtils cond: {
	    {[aStringOrCollection isKindOf: String]. [
	        path := aStringOrCollection asFileReference.
	        elFiles := self readFileOrFolder: path.
	    ]}.
	    {[aStringOrCollection isKindOf: Collection]. [
	        aStringOrCollection do: [:aPath |
	            elFiles addAll: self readFileOrFolder: aPath asFileReference.
	        ]
	    ]}.
	    {true. [self error: 'A String or Collection was expected, but got a ', aStringOrCollection class asString]}
	}.
    elFiles do: [:file |
	    | fuelPath obj contents serializer crc |
	    fuelPath := (file pathString, '.fuel').
	    (fuelPath asFileReference exists and: [fuelPath asFileReference modificationTime > file modificationTime and: [(Eigtils inFuelFile: fuelPath atHeaderEntry: #crc16) = (crc := CRC crc16FromCollection: (contents := file contents))]])
	        ifTrue: [
	            [
	                obj := Eigtils loadFuelFile: fuelPath.
	                "self context mergeGlobalsWith:
	                    (Eigtils inFuelFile: fuelPath atHeaderEntry: #globals).
	                self context mergeReferencesWith:
	                    (Eigtils inFuelFile: fuelPath atHeaderEntry: #references)."
	            ]
	            on: FLMethodChanged
	            do: [
	                fuelPath asFileReference delete.
	                obj := self readAndCreateFuelFrom: file.
	            ]
	        ]
	        ifFalse: [
	            obj := self readAndCreateFuelFrom: file.
	        ].
	    self initObjectRecursively: obj.
	    obj context: context; parent: fileColl.
	    fileCache add: obj.
	].
	^ cachedExpr := fileColl
	    context: context;
	    value: fileCache;
	    parent: false;
	    yourself.
]
