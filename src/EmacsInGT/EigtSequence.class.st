"
ELisp sequence representation.
"
Class {
	#name : #EigtSequence,
	#superclass : #EigtObject,
	#instVars : [
		'firstChildPos'
	],
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtSequence >> = anObject [
    ^ anObject class = self class
    and: [(anObject children size = self children size)
    and: [
        | eqs |
        eqs := Array new: self children size.
        1 to: self children size do: [:index |
            eqs at: index put: ((self children at: index) = (anObject children at: index))
        ].
        eqs allSatisfy: [:eq | eq].
    ]].
]

{ #category : #accessing }
EigtSequence >> _gtObjectsFor: aView withTitle: aString [
	^ aView columnedList
		title: (aString) translated;
		priority: 50;
		items: [ ([self value] on: Error do: [{self car . self cdr}]) asOrderedCollection ];
		column: (aString) text: [ :eachItem | eachItem gtDisplayText ];
		actionUpdateButtonTooltip: 'Update list'.
]

{ #category : #accessing }
EigtSequence >> children [
    ^ ([self value] on: Error do: [{self car . self cdr}]).
]

{ #category : #accessing }
EigtSequence >> context: anIntegerOrEigtContext [
    "Update context for children objects, if there are any."
    super context: anIntegerOrEigtContext.
    [([self value] on: Error do: [{self car . self cdr}])
        do: [:child | child ifNotNil: [child context: anIntegerOrEigtContext]]]
        on: MessageNotUnderstood do: [nil].
]

{ #category : #accessing }
EigtSequence >> gtCombinedFor: aView [
    <gtView>
    | srcEd |
	^ aView explicit
        title: 'Combined';
        priority: 3;
        actionButtonIcon: BrGlamorousVectorIcons playinspect
			tooltip: 'Evaluate'
			action: [:button | button phlow spawnObject: self eval ];
		actionButtonIcon: BrGlamorousVectorIcons transcript
			tooltip: 'Print'
			action: [:button | button phlow spawnObject: self eval print ];
		actionButtonIcon: BrGlamorousVectorIcons upwards
			tooltip: 'Parent'
			action: [:button | self parent ifNotNil: [button phlow spawnObject: self parent] ];
		stencil: [
            | aContainer |
            aContainer := BlElement new 
                layout: BlLinearLayout vertical;
                constraintsDo: [:c | c vertical matchParent. c horizontal matchParent].
            srcEd := (self gtSourceCodeFor: aView) asElement.
            srcEd constraintsDo: [:c | c vertical matchParent. c horizontal matchParent].
            "srcEd addChild: (BrResizer new
                beBottom;
                target: srcEd;
                aptitude: BrGlamorousResizerAptitude;
                elevation: (BlRelativeElevation elevation: 1000);
                constraintsDo: [ :c | c ignoreByLayout ])."
            aContainer addChild: srcEd.
            aContainer addChild: ((self gtObjectsFor: aView) asElement).
        ]
]

{ #category : #accessing }
EigtSequence >> gtObjectsFor: aView [
    <gtView>
    ^ self _gtObjectsFor: aView withTitle: 'Objects'
]

{ #category : #accessing }
EigtSequence >> initialize [
    super initialize.
    firstChildPos := 1.
]

{ #category : #accessing }
EigtSequence >> setChildrenParentsAndStarts [
    | nextPos |
	firstChildPos ifNil: [self error: 'First position not initialized'].
	nextPos := firstChildPos.
    self children do: [:child |
        child parent ifNil: [child parent: self].
        child start ifNil: [child start: nextPos].
        child stop ifNil: [child stop: nextPos + child print size].
        child context: context.
        nextPos := nextPos + child print size + 1.
    ].
]

{ #category : #accessing }
EigtSequence >> start [
    ^ start ifNil: [1] ifNotNil: [start].
]

{ #category : #accessing }
EigtSequence >> stop [
    stop ifNotNil: [^ stop].
    ^ parent ifNil: [self print size] ifNotNil: [self start + self print size - 1].
]

{ #category : #accessing }
EigtSequence >> value: aCollection [
    value := aCollection.
    self setChildrenParentsAndStarts.
]
