Class {
	#name : #EigtDebugNode,
	#superclass : #GeaAbstractDebugNode,
	#category : #'EmacsInGT-Utilities'
}

{ #category : #accessing }
EigtDebugNode >> isValid [
    ^ item isNotNil and: [item class ~= EigtFile].
]

{ #category : #accessing }
EigtDebugNode >> location [
    ^ item.
]

{ #category : #accessing }
EigtDebugNode >> nodeSrc [
    ^ self source copyFrom: self start to: self stop.
]

{ #category : #accessing }
EigtDebugNode >> printLocation [
    ^ '<string>:{1}:{2}' format: {self start . self stop}.
]

{ #category : #accessing }
EigtDebugNode >> printOn: aStream [
    super printOn: aStream.
    aStream
        nextPutAll: ' ({1})' format: {item printString}.
]

{ #category : #accessing }
EigtDebugNode >> source [
    ^ item source.
]

{ #category : #accessing }
EigtDebugNode >> start [
    ^ item start.
]

{ #category : #accessing }
EigtDebugNode >> stop [
    ^ item stop.
]
