Class {
	#name : #EigtDebugContext,
	#superclass : #GeaAbstractDebugContext,
	#instVars : [
		'expr',
		'context'
	],
	#category : #'EmacsInGT-Utilities'
}

{ #category : #accessing }
EigtDebugContext class >> from: anObject [
    ^ self new from: anObject. 
]

{ #category : #accessing }
EigtDebugContext >> expr [
    ^ expr.
]

{ #category : #accessing }
EigtDebugContext >> from: anObject [
    expr := anObject.
]
