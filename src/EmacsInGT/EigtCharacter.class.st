Class {
	#name : #EigtCharacter,
	#superclass : #EigtInteger,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtCharacter class >> ast: anELispCharacterNode [
    "TODO: implement native conversion"
    | inst aValue |
    aValue := EmacsServerInterface new eval: anELispCharacterNode value value.
    inst := EigtCharacter new
        value: aValue asNumber;
        ast: anELispCharacterNode;
        start: anELispCharacterNode value startPosition;
        stop: anELispCharacterNode value stopPosition.
	^ inst
]

{ #category : #accessing }
EigtCharacter >> value [
    ^ value.
]

{ #category : #accessing }
EigtCharacter >> value: anIntegerOrCharacter [
    | anInteger |
	anInteger := anIntegerOrCharacter class = Character 
	    ifTrue: [anInteger := anIntegerOrCharacter asUnicode]
	ifFalse: [anInteger := anIntegerOrCharacter].
    (anInteger isKindOf: Integer) ifFalse: [self error: 'Value must be given as integer or character'].
    value := anInteger.
]
