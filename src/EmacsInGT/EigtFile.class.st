"
I am the ultimate container for an expression object read from a string.
"
Class {
	#name : #EigtFile,
	#superclass : #EigtSequence,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtFile class >> value: aCollection [
    ^ self new value: aCollection.
]

{ #category : #accessing }
EigtFile >> _eval [
    "NOTE: We only return the last item because only the result of the last in a series is usually returned after evaluation."
    | last |
	^ (value collect: [:el |
        el context: context; parent: self.
        el eval.
    ]) last.
]

{ #category : #accessing }
EigtFile >> context: anEigtContext [
    context := anEigtContext.
]

{ #category : #accessing }
EigtFile >> eval [
    | result |
	Eigtils runBeforeEvalBlockIn: meta on: self.
	result := self _eval.
    Eigtils runAfterEvalBlockIn: meta on: self.
    ^ result.
]

{ #category : #accessing }
EigtFile >> initialize [
    super initialize.
    firstChildPos := 1.
    parent := false.
]

{ #category : #accessing }
EigtFile >> print [
    "Return printable of the last item."
    ^ value last print.
]

{ #category : #accessing }
EigtFile >> source [
    ^ source ifNil: [' ' join: (value collect: [:eo | eo print])].
]

{ #category : #accessing }
EigtFile >> source: aString [
    source := aString.
]

{ #category : #accessing }
EigtFile >> start [
    ^ 1.
]

{ #category : #accessing }
EigtFile >> stop [
    ^ 1.
]
