Class {
	#name : #EigtDebugEvalable,
	#superclass : #GeaAbstractDebugEvalable,
	#instVars : [
		'expr'
	],
	#category : #'EmacsInGT-Utilities'
}

{ #category : #accessing }
EigtDebugEvalable >> expr [
    "self error: 'Send #evalable instead'."
    ^ evalable.
]

{ #category : #accessing }
EigtDebugEvalable >> expr: anEigtObject [
    "self error: 'Send to #evalable: instead'."
    evalable := anEigtObject.
]

{ #category : #accessing }
EigtDebugEvalable >> sourceNode: aSession [
    ^ EigtDebugNode from: evalable.
]
