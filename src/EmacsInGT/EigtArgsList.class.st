"
My purpose is to wrap arguments into a single object in order to bypass the `#perform:withArguments:` numArgs check.
"
Class {
	#name : #EigtArgsList,
	#superclass : #Object,
	#instVars : [
		'arguments',
		'context',
		'numVars'
	],
	#category : #'EmacsInGT-Utilities'
}

{ #category : #accessing }
EigtArgsList class >> createAndPut: aCollection withContext: anEigtContext [
    self notYetImplemented
]

{ #category : #accessing }
EigtArgsList class >> from: aCollection context: anEigtContext [
    ^ EigtArgsList new
        put: aCollection;
        context: anEigtContext;
        yourself.
]

{ #category : #accessing }
EigtArgsList class >> withContext: anEigtContextHolder andPut: aCollection [
    ""
    ^ self new
        context: anEigtContextHolder context;
        put: aCollection;
        yourself.
]

{ #category : #accessing }
EigtArgsList >> context [
    ^ EigtEngine getContextAt: context.
]

{ #category : #accessing }
EigtArgsList >> context: anEigtContext [
    context := anEigtContext isNumber
        ifTrue: [anEigtContext]
        ifFalse: [anEigtContext identityHash].
]

{ #category : #accessing }
EigtArgsList >> doesNotUnderstand: aMessage [
    self error: 'It seems this argument object was not unwrapped before use'.
]

{ #category : #accessing }
EigtArgsList >> fifth [
    ^arguments fifth.
]

{ #category : #accessing }
EigtArgsList >> first [
    ^ arguments first.
]

{ #category : #accessing }
EigtArgsList >> fourth [
    arguments fourth.
]

{ #category : #accessing }
EigtArgsList >> get [
    ^ arguments do: [:arg | arg context: context].
]

{ #category : #accessing }
EigtArgsList >> getVars: anEigtCons [
    "Associate arguments with variables in using function.
    
    TODO: Validate arguments against variables.
        - Handle less args for opt and rest (pad arguments with nils?).
    "
    | posVars optVars restVar optSplit restSplit point varsColl argsRead varNames marks |
	point := 0.
    varsColl := anEigtCons value.
    argsRead := 0.
    marks := #('&optional' '&rest').
    varNames := varsColl collect: [:var | var symbolName].
    varsColl do: [:var |
        (marks contains: [:mark | mark = var symbolName])
            ifTrue: [point := point + 1]
            ifFalse: [
                argsRead := argsRead + 1.
                varsColl last ~= var
                    ifTrue: [argsRead <= arguments size
                        ifTrue: [var lexicalSet: (arguments at: argsRead)]
                        ifFalse: [var lexicalSet: self context nil]
                    ]
                    ifFalse: [argsRead > arguments size
                        ifTrue: [var lexicalSet: self context nil] 
                        ifFalse: [var lexicalSet: EigtCons value: (arguments copyFrom: argsRead to: arguments size)]
                    ].
            ]
    ].
]

{ #category : #accessing }
EigtArgsList >> gtArgumentsFor: aView [
	<gtView>
	arguments ifNil: [^ aView empty].
	^ aView columnedList
		title: 'Arguments List' translated;
		priority: 50;
		items: [ (arguments) asOrderedCollection ];
		column: 'Arguments' text: [ :eachItem | eachItem gtDisplayText ];
		actionUpdateButtonTooltip: 'Update list'.
]

{ #category : #accessing }
EigtArgsList >> gtVariablesFor: aView [
	"<gtView>"
	arguments ifNil: [^ aView empty].
	^ aView columnedList
		title: 'Variables List' translated;
		priority: 50;
		items: [ (arguments) asOrderedCollection ];
		column: 'Arguments' text: [ :eachItem | eachItem gtDisplayText ];
		actionUpdateButtonTooltip: 'Update list'.
]

{ #category : #accessing }
EigtArgsList >> opt [
    ^ self error: 'Not implemented'.
]

{ #category : #accessing }
EigtArgsList >> pos [ 
    ^ self error: 'Not implemented'.
]

{ #category : #accessing }
EigtArgsList >> printOn: aStream [
	super printOn: aStream.
	aStream nextPutAll: '<', (' ' join: (arguments collect: [:a | a asString])), '>'.
]

{ #category : #accessing }
EigtArgsList >> put: args [
    self set: args.
]

{ #category : #accessing }
EigtArgsList >> rest [
    ^ self error: 'Not implemented'.
]

{ #category : #accessing }
EigtArgsList >> second [
    ^ arguments second.
]

{ #category : #accessing }
EigtArgsList >> set: aCollection [
    (aCollection isKindOf: Collection) ifFalse: [self error: 'EigtArguments only holds collections'].
    arguments := aCollection.
]

{ #category : #accessing }
EigtArgsList >> setVars: anEigtCons [
    "Associate arguments with variables in using function.
    
    TODO: Validate arguments against variables.
        - Handle less args for opt and rest (pad arguments with nils?).
    "
    | posVars optVars restVar optSplit restSplit point varsColl argsRead varNames marks markPoint |
    point := 0.
    varsColl := anEigtCons value.
    argsRead := 0.
    marks := #('&optional' '&rest').
    markPoint := '&pos'.
    varsColl do: [:var |
        (marks contains: [:mark | mark = var symbolName])
            ifTrue: [point := point + 1. markPoint := var symbolName]
            ifFalse: [
                argsRead := argsRead + 1.
                self context pushSymbolValueFor: var.
                (varsColl last = var and: [markPoint = marks second])
                    ifTrue: [argsRead > arguments size
                        ifTrue: [var lexicalSet: self context nil] 
                        ifFalse: [var lexicalSet: (EigtCons new context: context; value: (arguments copyFrom: argsRead to: arguments size); yourself)]
                    ]
                    ifFalse: [argsRead <= arguments size
                        ifTrue: [var lexicalSet: (arguments at: argsRead)]
                        ifFalse: [var lexicalSet: self context nil]
                    ].
            ]
    ].
    numVars := varsColl size - point.
    "1 to: arguments size do: [:idx | 
        
        point = #pos ifTrue: [posVars at: (varsColl at: idx) symbolName put: (arguments at: idx)]
    ]."
]

{ #category : #accessing }
EigtArgsList >> third [
    ^ arguments third.
]

{ #category : #accessing }
EigtArgsList >> unsetVars [
    self context popSymbolValues: numVars.
]
