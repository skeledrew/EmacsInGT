Class {
	#name : #ELispExpressionNode,
	#superclass : #SmaCCParseNode,
	#category : #'EmacsInGT-AST'
}

{ #category : #generated }
ELispExpressionNode >> acceptVisitor: anExpressionVisitor [
	^ anExpressionVisitor visitExpression: self
]
