"
I represent an ELisp symbol datatype. I am copied when shown in multiple places in source code, and use EigtRef to ensure I still point to the primary data I contain.
"
Class {
	#name : #EigtSymbol,
	#superclass : #EigtAtom,
	#instVars : [
		'pListCell',
		'functionCell',
		'nameCell',
		'valueCell'
	],
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtSymbol class >> ast: anELispSymbolNode [
    "TODO: Recheck this."
    | inst |
	inst := super ast: anELispSymbolNode.
    inst value: anELispSymbolNode name value asSymbol.
    ^ inst.
]

{ #category : #accessing }
EigtSymbol class >> findOrCreateFromAst: aSymbolNode withContext: anEigtContext [
    "If aSymbol doesn't exist, create and add to anEigtContext.
    
    TODO: Find why suddenly symbol values have a space in them.
    "
    | aSymbol es aNameSymbol |
    aNameSymbol := aSymbolNode name value trim asSymbol.
    aSymbol := (EigtEngine getContextAt: anEigtContext) lookup: aNameSymbol for: #any.
    ^ aSymbol ifNil: [
        es := EigtSymbol new 
            withName: aNameSymbol
            andContext: anEigtContext;
            ast: aSymbolNode;
            start: aSymbolNode name startPosition;
            stop: aSymbolNode name stopPosition;
            yourself.
        (EigtEngine getContextAt: anEigtContext) inNamespaceAt: aNameSymbol put: es.
        ^ es.
    ] ifNotNil: [
        aSymbol copy
            ast: aSymbolNode;
            start: aSymbolNode name startPosition;
            stop: aSymbolNode name stopPosition;
            yourself.
    ].
]

{ #category : #accessing }
EigtSymbol class >> findOrCreateWithContext: anEigtContext andName: aNameString [
    "If aSymbol doesn't exist, create and add to anEigtContext."
    | aSymbol es aNameSymbol |
    aNameSymbol := aNameString asSymbol.
    aSymbol := (EigtEngine getContextAt: anEigtContext) lookup: aNameSymbol for: #any.
    aSymbol ifNil: [
        es := EigtSymbol new 
            withName: aNameSymbol
            andContext: anEigtContext;
            yourself.
        (EigtEngine getContextAt: anEigtContext) inNamespaceAt: aNameSymbol put: es.
        ^ es.
    ] ifNotNil: [^ aSymbol copy].
]

{ #category : #accessing }
EigtSymbol >> = anObject [
    ^ anObject class = self class
    and: [{'Name'. 'Function'. 'PList'. 'Value'} allSatisfy: [:frag |
        (self perform: ('symbol', frag) asSymbol)
        = (anObject perform: ('symbol', frag) asSymbol)
    ]]
]

{ #category : #accessing }
EigtSymbol >> _eval [
    "Attempt to resolve the symbol to a bound - or lexical - value if not quoted."
    | lexicalValue |
	(({self context t. self context nil} contains: [:obj | obj = self]) or: nameCell first = $:)
        ifTrue: [^ self].
    (self context lexicalBinding = true and: [self isSpecial ~= true]) ifTrue: [
        lexicalValue := self findLexicalValue.
        lexicalValue ifNotNil: [^ lexicalValue].
    ].
    valueCell value ifNil: [self error: 'Symbol''s value as variable is void: ', nameCell].
    ^ valueCell value.
]

{ #category : #accessing }
EigtSymbol >> ast: anELispSymbolNode [
    ""
    ast := anELispSymbolNode.
]

{ #category : #accessing }
EigtSymbol >> callWith: aList [
    "Attempt to resolve the symbol as a function."
    self deprecated: 'This seems obsolete'.
    functionCell value ifNil: [self error: 'Symbol''s function definition is void: ', nameCell].
    self notImplemented.
]

{ #category : #accessing }
EigtSymbol >> context: anEigtContext [
    "Set the context (environment) for this symbol."
    | ctxId |
	ctxId := anEigtContext isNumber
        ifTrue: [anEigtContext]
        ifFalse: [anEigtContext identityHash].
    context := ctxId.
    functionCell context: ctxId.
    pListCell context: ctxId.
    valueCell context: ctxId.
]

{ #category : #accessing }
EigtSymbol >> eval [
    | result |
	Eigtils runBeforeEvalBlockIn: meta on: self.
    result := self _eval.
    Eigtils runAfterEvalBlockIn: meta on: self.
    ^ result.
]

{ #category : #accessing }
EigtSymbol >> findLexicalValue [
    "Search binding parents for lexically bound value."
    | aLexValue aParent |
	aLexValue := nil.
    aParent := self parent.
    [aParent = false or: [aLexValue isNotNil]] whileFalse: [
        (aParent isKindOf: EigtSequence) ifFalse: [
            "self error: 'aParent was not properly initialized'"
            aParent := EigtVoid new
        ]. "NOTE: This error is for catching cases where a parent did not properly set the children"
        (aParent isEigtCons and: [aParent lexicalNS notNil]) ifTrue: [
            aLexValue := aParent lexicalNS at: nameCell ifAbsent: nil.
        ].
        aParent := aParent parent.
    ].
    ^ aLexValue.
]

{ #category : #accessing }
EigtSymbol >> fset: anEigtObject [
    anEigtObject parent isNil ifTrue: [
        anEigtObject parent: false.
        anEigtObject deep: #children do: [:child |
	        "child context: context."
	        (child isKindOf: EigtSequence) ifTrue: [
	            child setChildrenParentsAndStarts
	        ]
	    ]. 
    ].
    ^ functionCell value: anEigtObject.
]

{ #category : #accessing }
EigtSymbol >> get: aPropertyEigtObject [
    1 to: pListCell value value size by: 2 do: [:idx | 
        (pListCell value value at: idx) = aPropertyEigtObject 
            ifTrue: [^ pListCell value value at: idx + 1]
    ].
    ^ self context nil.
]

{ #category : #accessing }
EigtSymbol >> gtFunctionFor: aView [
	<gtView>
	self symbolFunction ifNil: [^ aView empty].
	aView priority: 12.
	^ (self symbolFunction isKindOf: EigtAtom)
	    ifTrue: [self symbolFunction _gtPreviewFor: aView withTitle: 'Function Cell']
	    ifFalse: [
	        self symbolFunction class = EigtSubrGroup
	            ifTrue: [aView empty]
	            ifFalse: [self symbolFunction _gtObjectsFor: aView withTitle: 'Function Cell']]
]

{ #category : #accessing }
EigtSymbol >> gtNameFor: aView [
	<gtView>
	^ aView explicit
		title: 'Name Cell';
		priority: 11;
		stencil: [ 
			| aNumberElement |
			aNumberElement := BrLabel new 
				text: self print;
				aptitude: BrGlamorousLabelAptitude + BrShadowAptitude;
				background: Color white;
				margin: (BlInsets all: 20);
				padding: (BlInsets all: 10);
				layout: BlLinearLayout horizontal;
				constraintsDo: [ :c | 
					c vertical fitContent.
					c horizontal fitContent.
					c frame horizontal alignCenter.
					c frame vertical alignCenter ].
			BlElement new 
				constraintsDo: [:c | 
					c vertical matchParent.
					c horizontal matchParent];
				layout: BlFrameLayout new;
				addChild: aNumberElement ]
]

{ #category : #accessing }
EigtSymbol >> gtPListFor: aView [
	<gtView>
	| data plcValue |
	data := OrderedCollection new.
	plcValue := pListCell value value.
	plcValue size < 2 ifTrue: [^ aView empty].
	1 to: plcValue size by: 2 do: [:idx | [data add: {plcValue at: idx . plcValue at: idx + 1}] on: Error do: [nil]].
	^ aView columnedList
		title: 'PList Cell';
		priority: 14;
		items: [ data ];
		column: 'Property' text: [ :pair | pair at: 1 ];
		column: 'Value' text: [ :pair | pair at: 2 ];
		send: [ :pair | pair ]
]

{ #category : #accessing }
EigtSymbol >> gtPreviewFor: aView [
	<gtView>
	| data |
	data := OrderedCollection new.
	"plcValue size < 2 ifTrue: [^ aView empty]."
	data add: {'Name' . nameCell}.
	functionCell ifNotNil: [data add: {'Function' . functionCell value}].
	valueCell ifNotNil: [data add: {'Value' . valueCell value}].
	pListCell value value size > 1 ifTrue: [data add: {'Properties' . pListCell value}].
	^ aView columnedTree
		title: 'Preview';
		priority: 1;
		items: [ data ];
		children: [ :each | 
			each value isDictionary
				ifTrue: [ each value associations ]
				ifFalse: [ (each value isArray and: [ each value allSatisfy: #isDictionary ])
						ifTrue: [ each value collectWithIndex: [ :x :i | i -> x ] ]
						ifFalse: [ #() ] ] ];
		column: 'Cell' text: [ :pair | pair first ];
		column: 'Content' text: [ :pair | pair second ];
		send: [ :pair | pair second ]
]

{ #category : #accessing }
EigtSymbol >> gtValueFor: aView [
	<gtView>
	self symbolValue ifNil: [^ aView empty].
	aView priority: 13.
	^ (self symbolValue isKindOf: EigtAtom)
	    ifTrue: [self symbolValue _gtPreviewFor: aView withTitle: 'Value Cell']
	    ifFalse: [self symbolValue _gtObjectsFor: aView withTitle: 'Value Cell']
]

{ #category : #accessing }
EigtSymbol >> initialize [
    super initialize.
    functionCell := EigtRef new.
    pListCell := EigtRef new.
    valueCell := EigtRef new.
    meta := Dictionary new.
]

{ #category : #accessing }
EigtSymbol >> isSpecial [
    "Return true if the symbol was created with defvar, defcutom or defalias."
    ^ meta at: #isSpecial ifAbsent: false.
]

{ #category : #accessing }
EigtSymbol >> isSpecial: aBoolean [
    meta at: #isSpecial put: aBoolean.
]

{ #category : #accessing }
EigtSymbol >> lexicalSet: anEigtObject [
    ""
    | aParent |
    aParent := self parent.
    [self context lexicalBinding not or: [aParent = false]] whileFalse: [
        (aParent isKindOf: EigtSequence) ifFalse: [self error: 'aParent was not properly initialized']. "NOTE: This error is for catching cases where a parent did not properly set the children"
        (aParent isEigtCons and: [aParent lexicalNS notNil]) ifTrue: [
            aParent lexicalNS at: nameCell put: anEigtObject.
            ^ anEigtObject.
        ].
        aParent := aParent parent.
    ].
    ^ self set: anEigtObject.
]

{ #category : #accessing }
EigtSymbol >> lookup: aSymbol [
    "Search from the most recent local namespace back to global for aSymbol.
    
    TODO: add differentiator between variable and function lookup.
    "
    | nss foundSymbol |
	self deprecated: 'Should be dead'.
	nss := context namespace.
	nss reverseDo: [:ns | 
	    foundSymbol := ns at: aSymbol ifAbsent: [nil]. 
	    foundSymbol ifNotNil: [^ foundSymbol].
	].
    ^ nil.
]

{ #category : #accessing }
EigtSymbol >> name: aSymbol [
    ""
    nameCell ifNotNil: [self error: 'Cannot change symbol representation'].
    value := aSymbol.
    nameCell := aSymbol asString.
    functionCell context: context; at: 'EigtSymbol::', nameCell asString, '::functionCell' put: nil.
    pListCell context: context; at: 'EigtSymbol::', nameCell asString, '::pListCell' put: (EigtCons nilWithContext: context).
    valueCell context: context; at: 'EigtSymbol::', nameCell asString, '::valueCell' put: nil.
    "pListCell value: (context
        ifNil: [EigtCons nil]
        ifNotNil: [EigtCons nilWithContext: context])."
]

{ #category : #accessing }
EigtSymbol >> postCopy [
    "Reset attributes which depend on individualized instances."
    parent := nil.
    start := nil.
    stop := nil.
]

{ #category : #accessing }
EigtSymbol >> print [
    ^ nameCell.
]

{ #category : #accessing }
EigtSymbol >> put: aValueEigtObject in: aPropertyEigtObject [
    "Add value to the property list
    
    TODO: Update the cell without recreating.
    "
    | curValue |
	curValue := pListCell value value.
	curValue ifNil: [curValue := OrderedCollection new].  "FIXME: dirty hack"
    1 to: curValue size by: 2 do: [:idx | 
        (curValue at: idx) = aPropertyEigtObject 
            ifTrue: [
                curValue at: idx + 1 put: aValueEigtObject.
                pListCell value: (EigtCons withContext: pListCell value context andValue: curValue).
                ^ aValueEigtObject.
            ]
    ].
    curValue addFirst: aValueEigtObject.
    curValue addFirst: aPropertyEigtObject.
    pListCell value: (EigtCons withContext: aPropertyEigtObject context andValue: curValue).
    ^ aValueEigtObject.
]

{ #category : #accessing }
EigtSymbol >> set: anEigtObject [
    "NOTE: Users are Eigt*SG>>#set, EigtContext>>popSymbolValue"
    ^ valueCell value: anEigtObject.
]

{ #category : #accessing }
EigtSymbol >> setq: anEigtArguments [
    | result args |
    args := anEigtArguments get.
	result := self set: args first eval.
	args size > 1
	    ifTrue: [^ args second setq: args allButFirst]
	    ifFalse: [^ result].
]

{ #category : #accessing }
EigtSymbol >> specialFormP [
    "Ref: https://www.gnu.org/software/emacs/manual/html_node/elisp/Special-Forms.html"
    (#('and' 'catch' 'cond' 'condition-case' 'defconst' 'defvar' 'function' 'if' 'interactive' 'lambda' 'let' 'let*' 'or' 'prog1' 'prog2' 'progn' 'quote' 'save-current-buffer' 'save-excursion' 'save-restriction' 'setq' 'setq-default' 'unwind-protect' 'while') 
        contains: [:sfs | sfs = nameCell]) ifTrue: [^ self context t].
    ^ self context nil.
]

{ #category : #accessing }
EigtSymbol >> symbolFunction [
    ""
    ^ functionCell value = self context nil 
        ifTrue: [EigtSubrGroup new name: nameCell; context: context] 
        ifFalse: [functionCell value].
]

{ #category : #accessing }
EigtSymbol >> symbolName [
    ""
    ^ nameCell.
]

{ #category : #accessing }
EigtSymbol >> symbolPList [
    ^ pListCell value.
]

{ #category : #accessing }
EigtSymbol >> symbolValue [
    ^ valueCell value.
]

{ #category : #accessing }
EigtSymbol >> value [
    self deprecated: 'EigtSymbol>>#value is obsolete and disabled'
]

{ #category : #accessing }
EigtSymbol >> withName: aSymbol andContext: anEigtContext [
    ""
    context := anEigtContext isNumber
        ifTrue: [anEigtContext]
        ifFalse: [anEigtContext identityHash].
    self name: aSymbol.
]
