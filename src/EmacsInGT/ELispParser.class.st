Class {
	#name : #ELispParser,
	#superclass : #SmaCCGLRParser,
	#category : #'EmacsInGT-AST'
}

{ #category : #generated }
ELispParser class >> ambiguousTransitions [
^#(
#[ 0 6 0 17] 
#[ 0 6 0 21] 
#[ 0 6 0 25] 
#[ 0 6 0 29] 
#[ 0 6 0 33] 
#[ 0 6 0 37] 
#[ 0 6 0 41] 
#[ 0 6 0 45] 
#[ 0 6 0 49] 
#[ 0 6 0 53] 
#[ 0 6 0 57] 
#[ 0 6 0 61] 
#[ 0 6 0 65]
	).
]

{ #category : #'generated-accessing' }
ELispParser class >> cacheId [
	^'2021-05-06T19:36:29.168632-05:00'
]

{ #category : #generated }
ELispParser class >> definitionComment [
"%glr;

%root Expression;
%prefix ELisp;
%suffix Node;
%id <number>;

<sign>
	: \+|\-
	;
<integer>
	: <sign>? (\d+ \.?)
	| ((\#o[0-7]+) | (\#x[0-9A-Fa-f]+) | (\#b[0-1]+) | (\#[1-3]?[0-9]r[\w\d]+)) \.*
	;
<float>
	: (<sign>)? ((\d+ \. \d+) | (\d* (\. \d*)? [Ee] <sign>? \d+))
	| <sign>? (\d+ \. \d+ [Ee] \+ (INF|NaN))
	;
<comment>
	: \; [^\r\n]*
	| \,	# commas are treated as whitespace, but whitespace is not saved, so I made them comments instead
	;
<qs>  # question mark + forward slash
	: \?\\
	;
<oc>  # open curly
	: \{
	;
<cc>  # close curly
	: \}
	;
<hexD>  # hexadecimal digit
	: [0-9A-Fa-f]
	;
#<allSymbolChars>
#	: !""\#$%&'\(\)\*\+\,\-\./\:\;<=>\?@\[\\\]\^_\{\|\}~
#	;
<character>
	: (<qs>\0-7]{3,3} | newline | return | space | tab | space | backspace | formfeed | \?\\?. | <qs>N<oc>[\w\s]+<cc> | <qs>N<oc><hexD>{1,8}<cc> | <qs>u<hexD>{4,4} | <qs>U<hexD>{8,8} | <qs>x<hexD>+ | <qs>[0-7]{1,3} | <qs>(\^|C\-)\w | <isUnicode> | (\?<isUnicode>) | (<qs>N\{U\+<hexD>+\}))
	| ((<qs>M-<isLetter>) | (<qs>M-\\[0-7]{3,3}) | (<qs>M-\\C-<isLetter>) | (<qs>\C-\\M-<isLetter>))
	| (\?(\\C-|\\M-|\\S-|\\H-|\\s-|\\A-){0,5}.)  # single char with any combo of mods
	;
<string>
	: "" <isEigtNotDoubleQuote>* ""
	;
#<firstSymbolChar>
#	: [a-zA-Z!$%&*\-_+=<>\.?/\|]
#	;
#<symbolChars>
#	: <firstSymbolChar> | \d | \# | \'
#	;
#<symbol>
#	: (<firstSymbolChar> (<symbolChars> | \: <symbolChars>)* \/)? <firstSymbolChar> (<symbolChars> | \: <symbolChars>)*
#	;
<firstSymbolChar>
	: <isEigtFirstSymbolChar>
	;
<symbolChars>
	: <firstSymbolChar> | \# | \? | \( | \[
	;
<safeSymbolChars>
	: ([a-zA-Z\+\-\*/_~!@$%\^&=\:<>\{\}] | <isUnicode>)
	;
<symbol>
	: ((<safeSymbolChars>)+ [^\)^\]])  # match symbols with a non-special start char
	| ([\d]+\.?[a-zA-Z\+\-]+.*^[\)\]])  # don't match integers
	| (\\ .*)  # match escaped anything
	| (\# [a-rt-zA-Z0-9] \( .* \) )  # don't match records
	| (\d+ (<safeSymbolChars>)+)  # match symbols starting with numbers
	| <safeSymbolChars>  # match a single symbol character
	;
<notHTSymbol>
	: (^(?:[^h]|h[^a]|ha[^s]|has[^h]|hash[^\-]|hash\-[^t]|hash\-t[^a]|hash\-ta[^b]|hash\-tab[^l]|hash\-tabl[^e]$) <symbol>+)
	;
<whitespace>
	: \s+
	;
File
	: Expressions {{}}
	;
Expression 
	: Integer
	| Float
	| String
	| Character
	| Symbol
	| Vector
	| Quote
	| Unquote
	| Record
	#| HashTable  # disabled until we can prevent it from conflicting with Record
	| Cons
	| Function
	;
Unquote
	: ("","" 'unquote' | "",@"" 'unquote') Expression 'value' {{}}
	;
Quote
	: (""'"" 'quote' | ""`"" 'quote') Expression 'value' {{}}
	;
Vector
	: ""["" 'leftBracket' Expressions ""]"" 'rightBracket' {{}}
	;
Expressions
	:
	| Expressions Expression 'expression'
	;
Symbol
	: <symbol> 'name' {{}}
	;
Character
	: <character> 'value' {{}}
	;
String
	: <string> 'value' {{}}
	;
Integer
	: <integer> 'value' {{}}
	;
Float
	: <float> 'value' {{}}
	;
Record
    : ""#s("" 'prelim' Symbol 'type' Expressions 'expressions' "")"" 'rightParen' {{}}
    ;
#HashTable
#    : ""#s(hash-table "" 'prelim' Expressions 'expressions' "")"" 'rightParen' {{}}
#    ;
Cons
    : ""("" 'leftParen' Expression 'car' ""."" 'dot' Expression 'cdr' "")"" 'rightParen' {{}}
    | ""("" 'leftParen' Expressions "")"" 'rightParen' {{}}
	#| ""nil"" 'value' {{}}  # NOTE: maybe this should be parsed as a symbol?
    ;
Function
	: ""#'"" 'hashQuote' (Cons 'value' | Symbol 'value' | Unquote 'value') {{}}
	;"
]

{ #category : #generated }
ELispParser class >> reduceTable [
^#(
	#(25 0 #reduceActionForExpressions1: 2659329 false ) 
	#(20 1 #reduceActionForFile1: 2211841 false ) 
	#(29 1 #reduceActionForInteger1: 2830337 false ) 
	#(30 1 #reduceActionForFloat1: 2868225 false ) 
	#(27 1 #reduceActionForCharacter1: 2752513 false ) 
	#(28 1 #reduceActionForString1: 2794497 false ) 
	#(26 1 #reduceActionForSymbol1: 2717697 false ) 
	#(25 2 #reduceActionForExpressions2: 2659330 false ) 
	#(21 1 #liftFirstValue: 2240520 false ) 
	#(21 1 #liftFirstValue: 2240519 false ) 
	#(21 1 #liftFirstValue: 2240518 false ) 
	#(21 1 #liftFirstValue: 2240517 false ) 
	#(21 1 #liftFirstValue: 2240516 false ) 
	#(21 1 #liftFirstValue: 2240515 false ) 
	#(21 1 #liftFirstValue: 2240513 false ) 
	#(21 1 #liftFirstValue: 2240514 false ) 
	#(21 1 #liftFirstValue: 2240521 false ) 
	#(21 1 #liftFirstValue: 2240522 false ) 
	#(21 1 #liftFirstValue: 2240523 false ) 
	#(33 2 #reduceActionForFunction3: 3335171 false ) 
	#(33 2 #reduceActionForFunction3: 3335170 false ) 
	#(33 2 #reduceActionForFunction3: 3335169 false ) 
	#(23 2 #reduceActionForQuote1: 2525185 false ) 
	#(22 2 #reduceActionForUnquote1: 2452481 false ) 
	#(22 2 #reduceActionForUnquote1: 2452482 false ) 
	#(23 2 #reduceActionForQuote1: 2525186 false ) 
	#(32 3 #reduceActionForCons2: 3101698 false ) 
	#(24 3 #reduceActionForVector1: 2590721 false ) 
	#(31 4 #reduceActionForRecord1: 2902017 false ) 
	#(32 5 #reduceActionForCons1: 3101697 false )
	).
]

{ #category : #generated }
ELispParser class >> scannerClass [
	^ELispScanner
]

{ #category : #generated }
ELispParser class >> startingStateForFile [
	^ 1
]

{ #category : #generated }
ELispParser class >> symbolNames [
	^ #('"#''"' '"#s("' '"''"' '"("' '")"' '","' '",@"' '"."' '"["' '"]"' '"`"' '<integer>' '<float>' '<comment>' '<character>' '<string>' '<symbol>' '<whitespace>' 'B e g i n' 'File' 'Expression' 'Unquote' 'Quote' 'Vector' 'Expressions' 'Symbol' 'Character' 'String' 'Integer' 'Float' 'Record' 'Cons' 'Function' '<number>' 'E O F' 'error')
]

{ #category : #generated }
ELispParser class >> symbolTypes [
	^ #(#SmaCCToken #SmaCCToken #SmaCCToken #SmaCCToken #SmaCCToken #SmaCCToken #SmaCCToken #SmaCCToken #SmaCCToken #SmaCCToken #SmaCCToken #SmaCCToken #SmaCCToken #SmaCCToken #SmaCCToken #SmaCCToken #SmaCCToken #SmaCCToken #ELispFileNode #ELispFileNode #ELispExpressionNode #ELispUnquoteNode #ELispQuoteNode #ELispVectorNode #OrderedCollection #ELispSymbolNode #ELispCharacterNode #ELispStringNode #ELispIntegerNode #ELispFloatNode #ELispRecordNode #ELispConsNode #ELispFunctionNode #SmaCCToken #SmaCCToken #SmaCCErrorNode)
]

{ #category : #generated }
ELispParser class >> transitionTable [
^#(
#[1 0 6 0 1 0 6 0 2 0 6 0 3 0 6 0 4 0 6 0 6 0 6 0 7 0 6 0 9 0 6 0 11 0 6 0 12 0 6 0 13 0 6 0 15 0 6 0 16 0 6 0 17 0 9 0 20 0 13 0 25 0 6 0 35] 
#[0 0 0 0 35] 
#[1 0 17 0 1 0 21 0 2 0 25 0 3 0 29 0 4 0 33 0 6 0 37 0 7 0 41 0 9 0 45 0 11 0 49 0 12 0 53 0 13 0 57 0 15 0 61 0 16 0 65 0 17 0 69 0 21 0 69 0 22 0 69 0 23 0 69 0 24 0 69 0 26 0 69 0 27 0 69 0 28 0 69 0 29 0 69 0 30 0 69 0 31 0 69 0 32 0 69 0 33 0 10 0 35] 
#[1 0 29 0 4 0 33 0 6 0 37 0 7 0 65 0 17 0 117 0 22 0 121 0 26 0 125 0 32] 
#[1 0 65 0 17 0 129 0 26] 
#[1 0 17 0 1 0 21 0 2 0 25 0 3 0 29 0 4 0 33 0 6 0 37 0 7 0 41 0 9 0 45 0 11 0 49 0 12 0 53 0 13 0 57 0 15 0 61 0 16 0 65 0 17 0 133 0 21 0 133 0 22 0 133 0 23 0 133 0 24 0 133 0 26 0 133 0 27 0 133 0 28 0 133 0 29 0 133 0 30 0 133 0 31 0 133 0 32 0 133 0 33] 
#[1 0 7 0 1 0 11 0 2 0 15 0 3 0 19 0 4 0 6 0 5 0 23 0 6 0 27 0 7 0 31 0 9 0 35 0 11 0 39 0 12 0 43 0 13 0 47 0 15 0 51 0 16 0 55 0 17 0 137 0 21 0 137 0 22 0 137 0 23 0 137 0 24 0 141 0 25 0 137 0 26 0 137 0 27 0 137 0 28 0 137 0 29 0 137 0 30 0 137 0 31 0 137 0 32 0 137 0 33] 
#[1 0 17 0 1 0 21 0 2 0 25 0 3 0 29 0 4 0 33 0 6 0 37 0 7 0 41 0 9 0 45 0 11 0 49 0 12 0 53 0 13 0 57 0 15 0 61 0 16 0 65 0 17 0 145 0 21 0 145 0 22 0 145 0 23 0 145 0 24 0 145 0 26 0 145 0 27 0 145 0 28 0 145 0 29 0 145 0 30 0 145 0 31 0 145 0 32 0 145 0 33] 
#[1 0 17 0 1 0 21 0 2 0 25 0 3 0 29 0 4 0 33 0 6 0 37 0 7 0 41 0 9 0 45 0 11 0 49 0 12 0 53 0 13 0 57 0 15 0 61 0 16 0 65 0 17 0 149 0 21 0 149 0 22 0 149 0 23 0 149 0 24 0 149 0 26 0 149 0 27 0 149 0 28 0 149 0 29 0 149 0 30 0 149 0 31 0 149 0 32 0 149 0 33] 
#[1 0 6 0 1 0 6 0 2 0 6 0 3 0 6 0 4 0 6 0 6 0 6 0 7 0 6 0 9 0 6 0 10 0 6 0 11 0 6 0 12 0 6 0 13 0 6 0 15 0 6 0 16 0 6 0 17 0 153 0 25] 
#[1 0 17 0 1 0 21 0 2 0 25 0 3 0 29 0 4 0 33 0 6 0 37 0 7 0 41 0 9 0 45 0 11 0 49 0 12 0 53 0 13 0 57 0 15 0 61 0 16 0 65 0 17 0 157 0 21 0 157 0 22 0 157 0 23 0 157 0 24 0 157 0 26 0 157 0 27 0 157 0 28 0 157 0 29 0 157 0 30 0 157 0 31 0 157 0 32 0 157 0 33] 
#[0 0 14 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 18 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 22 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 26 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 30 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 34 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 38 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 42 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 46 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 50 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 54 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 58 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 62 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 66 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 70 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 74 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 78 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 82 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 86 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 90 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[1 0 6 0 1 0 6 0 2 0 6 0 3 0 6 0 4 0 6 0 5 0 6 0 6 0 6 0 7 0 6 0 9 0 6 0 11 0 6 0 12 0 6 0 13 0 6 0 15 0 6 0 16 0 6 0 17 0 161 0 25] 
#[0 0 94 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 165 0 8] 
#[1 0 17 0 1 0 21 0 2 0 25 0 3 0 29 0 4 0 169 0 5 0 33 0 6 0 37 0 7 0 41 0 9 0 45 0 11 0 49 0 12 0 53 0 13 0 57 0 15 0 61 0 16 0 65 0 17 0 69 0 21 0 69 0 22 0 69 0 23 0 69 0 24 0 69 0 26 0 69 0 27 0 69 0 28 0 69 0 29 0 69 0 30 0 69 0 31 0 69 0 32 0 69 0 33] 
#[0 0 98 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 102 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[1 0 17 0 1 0 21 0 2 0 25 0 3 0 29 0 4 0 33 0 6 0 37 0 7 0 41 0 9 0 173 0 10 0 45 0 11 0 49 0 12 0 53 0 13 0 57 0 15 0 61 0 16 0 65 0 17 0 69 0 21 0 69 0 22 0 69 0 23 0 69 0 24 0 69 0 26 0 69 0 27 0 69 0 28 0 69 0 29 0 69 0 30 0 69 0 31 0 69 0 32 0 69 0 33] 
#[0 0 106 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[1 0 17 0 1 0 21 0 2 0 25 0 3 0 29 0 4 0 177 0 5 0 33 0 6 0 37 0 7 0 41 0 9 0 45 0 11 0 49 0 12 0 53 0 13 0 57 0 15 0 61 0 16 0 65 0 17 0 69 0 21 0 69 0 22 0 69 0 23 0 69 0 24 0 69 0 26 0 69 0 27 0 69 0 28 0 69 0 29 0 69 0 30 0 69 0 31 0 69 0 32 0 69 0 33] 
#[1 0 17 0 1 0 21 0 2 0 25 0 3 0 29 0 4 0 33 0 6 0 37 0 7 0 41 0 9 0 45 0 11 0 49 0 12 0 53 0 13 0 57 0 15 0 61 0 16 0 65 0 17 0 181 0 21 0 181 0 22 0 181 0 23 0 181 0 24 0 181 0 26 0 181 0 27 0 181 0 28 0 181 0 29 0 181 0 30 0 181 0 31 0 181 0 32 0 181 0 33] 
#[0 0 110 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 114 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 118 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35] 
#[0 0 185 0 5] 
#[0 0 122 0 1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0 11 0 12 0 13 0 15 0 16 0 17 0 35]
	).
]

{ #category : #accessing }
ELispParser >> gtStreamFor: aView [
    <gtView>
    ^ scanner gtStreamFor: aView.
]

{ #category : #'generated-reduction actions' }
ELispParser >> reduceActionForCharacter1: nodes [
	| result |
	result := ELispCharacterNode new.
	result value: (nodes at: 1).
	^ result
]

{ #category : #'generated-reduction actions' }
ELispParser >> reduceActionForCons1: nodes [
	| result |
	result := ELispConsNode new.
	result leftParen: (nodes at: 1).
	result car: (nodes at: 2).
	result dot: (nodes at: 3).
	result cdr: (nodes at: 4).
	result rightParen: (nodes at: 5).
	^ result
]

{ #category : #'generated-reduction actions' }
ELispParser >> reduceActionForCons2: nodes [
	| result |
	result := ELispConsNode new.
	result leftParen: (nodes at: 1).
	result addNodes: (nodes at: 2) to: result expressions.
	result rightParen: (nodes at: 3).
	^ result
]

{ #category : #'generated-reduction actions' }
ELispParser >> reduceActionForExpressions1: nodes [
	| result |
	result := OrderedCollection new: 2.
	^ result
]

{ #category : #'generated-reduction actions' }
ELispParser >> reduceActionForExpressions2: nodes [
	| result |
	result := nodes at: 1.
	self add: (nodes at: 2) to: result.
	^ result
]

{ #category : #'generated-reduction actions' }
ELispParser >> reduceActionForFile1: nodes [
	| result |
	result := ELispFileNode new.
	result addNodes: (nodes at: 1) to: result expressions.
	^ result
]

{ #category : #'generated-reduction actions' }
ELispParser >> reduceActionForFloat1: nodes [
	| result |
	result := ELispFloatNode new.
	result value: (nodes at: 1).
	^ result
]

{ #category : #'generated-reduction actions' }
ELispParser >> reduceActionForFunction3: nodes [
	| result |
	result := ELispFunctionNode new.
	result hashQuote: (nodes at: 1).
	result value: (nodes at: 2).
	^ result
]

{ #category : #'generated-reduction actions' }
ELispParser >> reduceActionForInteger1: nodes [
	| result |
	result := ELispIntegerNode new.
	result value: (nodes at: 1).
	^ result
]

{ #category : #'generated-reduction actions' }
ELispParser >> reduceActionForQuote1: nodes [
	| result |
	result := ELispQuoteNode new.
	result quote: (nodes at: 1).
	result value: (nodes at: 2).
	^ result
]

{ #category : #'generated-reduction actions' }
ELispParser >> reduceActionForRecord1: nodes [
	| result |
	result := ELispRecordNode new.
	result prelim: (nodes at: 1).
	result type: (nodes at: 2).
	result addNodes: (nodes at: 3) to: result expressionses.
	result rightParen: (nodes at: 4).
	^ result
]

{ #category : #'generated-reduction actions' }
ELispParser >> reduceActionForString1: nodes [
	| result |
	result := ELispStringNode new.
	result value: (nodes at: 1).
	^ result
]

{ #category : #'generated-reduction actions' }
ELispParser >> reduceActionForSymbol1: nodes [
	| result |
	result := ELispSymbolNode new.
	result name: (nodes at: 1).
	^ result
]

{ #category : #'generated-reduction actions' }
ELispParser >> reduceActionForUnquote1: nodes [
	| result |
	result := ELispUnquoteNode new.
	result unquote: (nodes at: 1).
	result value: (nodes at: 2).
	^ result
]

{ #category : #'generated-reduction actions' }
ELispParser >> reduceActionForVector1: nodes [
	| result |
	result := ELispVectorNode new.
	result leftBracket: (nodes at: 1).
	result addNodes: (nodes at: 2) to: result expressions.
	result rightBracket: (nodes at: 3).
	^ result
]
