Class {
	#name : #ELispUnquoteNode,
	#superclass : #ELispExpressionNode,
	#instVars : [
		'unquote',
		'value'
	],
	#category : #'EmacsInGT-AST'
}

{ #category : #generated }
ELispUnquoteNode >> acceptVisitor: anExpressionVisitor [
	^ anExpressionVisitor visitUnquote: self
]

{ #category : #generated }
ELispUnquoteNode >> nodeVariables [
	^ #(#value)
]

{ #category : #generated }
ELispUnquoteNode >> tokenVariables [
	^ #(#unquote)
]

{ #category : #generated }
ELispUnquoteNode >> unquote [
	^ unquote
]

{ #category : #generated }
ELispUnquoteNode >> unquote: aSmaCCToken [
	unquote := aSmaCCToken
]

{ #category : #generated }
ELispUnquoteNode >> value [
	^ value
]

{ #category : #generated }
ELispUnquoteNode >> value: anELispExpressionNode [
	self value notNil
		ifTrue: [ self value parent: nil ].
	value := anELispExpressionNode.
	self value notNil
		ifTrue: [ self value parent: self ]
]
