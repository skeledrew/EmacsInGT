Class {
	#name : #EigtString,
	#superclass : #EigtArray,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtString class >> ast: anELispStringNode [
    | inst |
	inst := super ast: anELispStringNode.
    inst
        value: anELispStringNode value value asString;
        start: anELispStringNode value startPosition;
        stop: anELispStringNode value stopPosition.
    ^ inst.
]

{ #category : #accessing }
EigtString >> children [
    ^ #().
]

{ #category : #accessing }
EigtString >> print [
    ^ '"', ('' join: (value collect: [:ele | ele value asCharacter])) , '"'.
]

{ #category : #accessing }
EigtString >> value [
    ^ value.
]

{ #category : #accessing }
EigtString >> value: aString [
    "Convert to a character collection."
    value := OrderedCollection new.
    aString allButFirst allButLast do: [:char | value add: (EigtCharacter new parent: self; value: char)].
]
