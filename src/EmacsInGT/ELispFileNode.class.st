Class {
	#name : #ELispFileNode,
	#superclass : #ELispExpressionNode,
	#instVars : [
		'expressions'
	],
	#category : #'EmacsInGT-AST'
}

{ #category : #generated }
ELispFileNode >> acceptVisitor: anExpressionVisitor [
	^ anExpressionVisitor visitFile: self
]

{ #category : #generated }
ELispFileNode >> compositeNodeVariables [
	^ #(#expressions)
]

{ #category : #generated }
ELispFileNode >> expressions [
	^ expressions
]

{ #category : #generated }
ELispFileNode >> expressions: anOrderedCollection [
	self setParents: self expressions to: nil.
	expressions := anOrderedCollection.
	self setParents: self expressions to: self
]

{ #category : #'generated-initialize-release' }
ELispFileNode >> initialize [
	super initialize.
	expressions := OrderedCollection new: 2.
]
