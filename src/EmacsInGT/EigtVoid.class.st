Class {
	#name : #EigtVoid,
	#superclass : #EigtAtom,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtVoid >> parent [
    ^ false.
]
