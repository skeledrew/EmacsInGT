Class {
	#name : #EigtDebugProcess,
	#superclass : #GeaAbstractDebugProcess,
	#instVars : [
		'currentExpr',
		'exprGenerator',
		'records'
	],
	#category : #'EmacsInGT-Utilities'
}

{ #category : #accessing }
EigtDebugProcess class >> forContext: aContext priority: anInteger [
    ^ self new.
]

{ #category : #accessing }
EigtDebugProcess class >> metalinkActionOn: reifs andUpdate: records [
    records add: {reifs object . reifs object context copyGlobals}.
]

{ #category : #accessing }
EigtDebugProcess >> debugActionDo: aDebugActionSymbol [
    "NOTE: We need to unpack here due to the generator yielding a pair."
    | debugItem |
	debugItem := self exprGenerator next.
	debugItem ifNil: [debugItem := {nil . Dictionary new}].
	currentExpr := debugItem first.
	session contextVariables: debugItem second.
]

{ #category : #accessing }
EigtDebugProcess >> evalable [
    ^ EigtDebugEvalable new
        expr: currentExpr;
        yourself.
]

{ #category : #accessing }
EigtDebugProcess >> exprGenerator [
    ^ exprGenerator ifNil: [exprGenerator := self makeExprGenerator_v3: context expr]
]

{ #category : #accessing }
EigtDebugProcess >> initialize [
    records := OrderedCollection new.
]

{ #category : #accessing }
EigtDebugProcess >> makeExprGenerator_v1: anEigtObject [
    "Second attempt.
    
    NOTE: Preserving as demo of strange behavior. When metalinks are already created and changes are made to the method, 2 versions of the method run, with the modified calling the unmodified via the link.
    "
    | eoClasses recBlock result mids |
	^ Generator on: [:gen |
        eoClasses := EigtObject allSubclasses select: [:cls | [cls methodDict at: #eval. true] on: Error do: [false]].
        eoClasses addFirst: EigtObject.
        records := OrderedCollection new.
        recBlock := [:reifs | self class metalinkActionOn: reifs andUpdate: records].
        mids := OrderedCollection new.
        eoClasses do: [:cls |
            result := Skeltils attach: recBlock to: cls>>#eval at: #after withReifsFor: #(object).
            SkelSingleRegistry at: result first put: true.
            mids add: result first.
        ].
        result := anEigtObject eval.
        mids do: [:mid | SkelSingleRegistry at: result first put: false].
        records do: [:record | gen yield: record].
    ].
]

{ #category : #accessing }
EigtDebugProcess >> makeExprGenerator_v1_5: anEigtObject [
    "Second attempt.
    
    
    "
    | eoClasses recBlock result mids |
	^ Generator on: [:gen |
        eoClasses := EigtObject allSubclasses select: [:cls | [cls methodDict at: #eval. true] on: Error do: [false]].
        eoClasses addFirst: EigtObject.
        records := OrderedCollection new.
        recBlock := [:reifs | self class metalinkActionOn: reifs andUpdate: records].
        mids := OrderedCollection new.
        eoClasses do: [:cls |
            result := Skeltils attach: recBlock to: cls>>#eval at: #after withReifsFor: #(object).
            SkelSingleRegistry at: result first put: true.
            mids add: result first.
        ].
        result := anEigtObject eval.
        mids do: [:mid | SkelSingleRegistry at: mid put: false].
        records do: [:record | gen yield: record].
    ].
]

{ #category : #accessing }
EigtDebugProcess >> makeExprGenerator_v2: anEigtObject [
    | eoClasses linkBlock result mids |
	^ Generator on: [:gen |
        eoClasses := EigtObject allSubclasses select: [:cls | [cls methodDict at: #eval. true] on: Error do: [false]].
        eoClasses addFirst: EigtObject.
        linkBlock := [:reifs |
            gen yield: {reifs object . reifs object context copyGlobals}].
        mids := OrderedCollection new.
        eoClasses do: [:cls |
            result := Skeltils attach: linkBlock to: cls>>#eval at: #after withReifsFor: #(object).
            SkelSingleRegistry at: result first put: true.
            mids add: result first.
        ].
        anEigtObject eval.
        mids do: [:mid | SkelSingleRegistry at: mid put: false].
    ].
]

{ #category : #accessing }
EigtDebugProcess >> makeExprGenerator_v3: anEigtObject [
    | eoFamily yieldBlock result mids |
	^ Generator on: [:gen |
        eoFamily := anEigtObject deepCollect: #children.
        eoFamily addFirst: anEigtObject.
        yieldBlock := [:eo |
            gen yield: {eo . eo context copyGlobals}].
        mids := OrderedCollection new.
        eoFamily do: [:eo |
            eo metaAt: #afterEvalBlock put: yieldBlock.
        ].
        [anEigtObject eval] on: Error do: [:exc | gen yield: {exc . Dictionary new}].
    ].
]

{ #category : #accessing }
EigtDebugProcess >> newDebugSessionNamed: aString startedAt: aContext [
    sessionName := aString.
    context := aContext.
    ^ session := EigtDebugSession named: aString on: self startedAt: aContext.
]
