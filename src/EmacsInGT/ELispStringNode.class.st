Class {
	#name : #ELispStringNode,
	#superclass : #ELispExpressionNode,
	#instVars : [
		'value'
	],
	#category : #'EmacsInGT-AST'
}

{ #category : #generated }
ELispStringNode >> acceptVisitor: anExpressionVisitor [
	^ anExpressionVisitor visitString: self
]

{ #category : #generated }
ELispStringNode >> tokenVariables [
	^ #(#value)
]

{ #category : #generated }
ELispStringNode >> value [
	^ value
]

{ #category : #generated }
ELispStringNode >> value: aSmaCCToken [
	value := aSmaCCToken
]
