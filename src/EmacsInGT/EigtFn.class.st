Class {
	#name : #EigtFn,
	#superclass : #EigtCons,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtFn >> children [
    ^ value.
]

{ #category : #accessing }
EigtFn >> eval [
    "TODO: Make a closure if lexical scoping active."
    ^ value first.
]

{ #category : #accessing }
EigtFn >> print [
    ^ quote, value first print.
]

{ #category : #accessing }
EigtFn >> value [
    ^ value first.
]

{ #category : #accessing }
EigtFn >> value: anObject [
    "Store a value who's type is representative of the object's class."
    value = nil
        ifTrue: [value := anObject]
        ifFalse: [self error: 'Value is write once.'].
]
