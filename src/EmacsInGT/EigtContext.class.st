"
I am the environment for an engine. I must be passed to all cons and symbols which it evaluates.
"
Class {
	#name : #EigtContext,
	#superclass : #Object,
	#instVars : [
		'globalNS',
		'localNS',
		'emacsServerInterface',
		'namespace',
		'primitives',
		'primitiveNS',
		'engine',
		'nativePrimitives',
		'symbolValueStack',
		'callDepth',
		'maxCallDepth',
		'quoteStack',
		'refValues',
		'timestamp'
	],
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtContext >> = anObject [
    ^ anObject class = EigtContext and: [self tsId = anObject tsId].
]

{ #category : #accessing }
EigtContext >> context [
    "NOTE: Convenience getter to avoid the check for use with holder."
    ^ self.
]

{ #category : #accessing }
EigtContext >> copy [
    self error: 'Context copying is not allowed'.
]

{ #category : #accessing }
EigtContext >> copyGlobals [
    ^ globalNS copy.
]

{ #category : #accessing }
EigtContext >> decreaseCallDepth [
    callDepth := callDepth - 1.
]

{ #category : #accessing }
EigtContext >> engine: anEigtEngine [
    engine := anEigtEngine.
]

{ #category : #accessing }
EigtContext >> evalOnServer: anELispString [
    ^ emacsServerInterface eval: anELispString.
]

{ #category : #accessing }
EigtContext >> evalWithEngine: anEigtObject [
    ^ engine eval: anEigtObject.
]

{ #category : #accessing }
EigtContext >> findSubr: aString [
    "Use a native subroutine's name to find the representative compiled method."
    EigtSignal emit: 'Seeking: ', aString, '; cid: ', self value identityHash asString.
    ^ nativePrimitives at: aString ifAbsent: [nil].
]

{ #category : #accessing }
EigtContext >> global: aSymbol [
    "Return the symbol's global value."
    ^ globalNS at: aSymbol ifAbsent: [self error: 'Not yet implemented']
]

{ #category : #accessing }
EigtContext >> gtGlobalsFor: aView [
	<gtView>
	^ aView columnedList
		title: 'Global Variables';
		priority: 11;
		items: [ globalNS associations sorted reject: [:assoc |
		    assoc value symbolValue isNil
		    & assoc value symbolFunction isNil
		    & assoc value symbolPList value isEmpty
		] ];
		column: 'Symbol' text: [ :assoc | assoc key ] width: 210;
		column: 'Function' text: [ :assoc | assoc value symbolFunction ];
		column: 'Value' text: [ :assoc | assoc value symbolValue ];
		column: 'PList' text: [ :assoc | assoc value symbolPList ];
		send: [ :assoc | assoc value ]
]

{ #category : #accessing }
EigtContext >> gtNativeSubrsFor: aView [
	<gtView>
	^ aView columnedList
		title: 'Native Subrs';
		priority: 14;
		items: [ nativePrimitives associations sorted ];
		"children: [ :each | 
			each value isDictionary
				ifTrue: [ each value associations ]
				ifFalse: [ (each value isArray and: [ each value allSatisfy: #isDictionary ])
						ifTrue: [ each value collectWithIndex: [ :x :i | i -> x ] ]
						ifFalse: [ #() ] ] ];"
		column: 'Name' text: [ :assoc | assoc key ];
		column: 'Method' text: [ :assoc | assoc value ];
		send: [ :assoc | assoc value ]
]

{ #category : #accessing }
EigtContext >> gtServerStatsFor: aView [
    <gtView>
    ^ (emacsServerInterface gtRunStatsFor: aView)
        priority: 15.
]

{ #category : #accessing }
EigtContext >> gtServerSubrsFor: aView [
	<gtView>
	^ aView columnedList
		title: 'Server Subrs';
		priority: 15;
		items: [ primitiveNS associations ];
		"children: [ :each | 
			each value isDictionary
				ifTrue: [ each value associations ]
				ifFalse: [ (each value isArray and: [ each value allSatisfy: #isDictionary ])
						ifTrue: [ each value collectWithIndex: [ :x :i | i -> x ] ]
						ifFalse: [ #() ] ] ];"
		column: 'Name' text: [ :assoc | assoc key ];
		column: 'Symbol' text: [ :assoc | assoc value ];
		send: [ :assoc | assoc value ]
]

{ #category : #accessing }
EigtContext >> gtSymbbolValuesStackFor: aView [
	<gtView>
	^ aView columnedList
		title: 'Symbol Values Stack';
		priority: 12;
		items: [ symbolValueStack ];
		"children: [ :each | 
			each value isDictionary
				ifTrue: [ each value associations ]
				ifFalse: [ (each value isArray and: [ each value allSatisfy: #isDictionary ])
						ifTrue: [ each value collectWithIndex: [ :x :i | i -> x ] ]
						ifFalse: [ #() ] ] ];"
		column: 'Symbol' text: [ :item | item first ];
		"column: 'Function' text: [ :assoc | assoc value symbolFunction ];"
		column: 'Value' text: [ :item | item second ];
		"column: 'PList' text: [ :assoc | assoc value symbolPList ];"
		send: [ :item | item second ]
]

{ #category : #accessing }
EigtContext >> inNamespaceAt: aSymbol [
    "Get an object from the most recent namespace."
    ^ namespace last at: aSymbol ifAbsent: [nil].
]

{ #category : #accessing }
EigtContext >> inNamespaceAt: aSymbol put: anEigtObject [
    "Add to the most recent namespace by default."
    namespace last at: aSymbol put: anEigtObject.
]

{ #category : #accessing }
EigtContext >> inQuote [
    ^ quoteStack size > 0.
]

{ #category : #accessing }
EigtContext >> increaseCallDepth [
    callDepth := callDepth + 1.
    callDepth > maxCallDepth ifTrue: [self error: 'Maximum call depth (', maxCallDepth asString, ') exceeded; infinite recursion?'].
]

{ #category : #accessing }
EigtContext >> initNativePrimitives [
	| pragmas key |
	nativePrimitives := Dictionary new.
	pragmas := (PragmaCollector
		filter: [ :prg | prg selector = 'eigtSubr:' ]) reset.
	pragmas
		do: [ :prg | 
			(prg arguments first isKindOf: String)
				ifFalse: [ self error: 
				    'Argument for pragma `', 
				    prg method methodClass name, 
				    '>>#', 
				    prg method selector, 
				    ' <', 
				    prg selector asString, 
				    '>` must be a string'
				].
			key := prg arguments first
				ifEmpty: [ prg method selector asString ].
			nativePrimitives add: key -> prg method 
        ].
    ^ nil.
]

{ #category : #accessing }
EigtContext >> initServerPrimitives [
    "Initialize symbols for primitive functions."
    | prims |
	prims := '(let ((primitives-list ''()))
     (mapatoms
      (lambda (sym)
        (if (subrp (symbol-function sym))
           (push sym primitives-list))))
     primitives-list)'.
     primitives := ((self evalOnServer: prims) allButFirst allButLast findTokens: ' ') collect: [:p | p asSymbol].
     primitives do: [:prim | primitiveNS at: prim put: (EigtSymbol new context: self; name: prim; fset: self nil; yourself)].
]

{ #category : #accessing }
EigtContext >> initialize [
    | ctxId |
	ctxId := EigtEngine registerContext: self.
    quoteStack := LinkedList new.
	refValues := Dictionary new.
    globalNS := EigtNamespace withContext: ctxId.
    primitiveNS := EigtNamespace withContext: ctxId.
    namespace := OrderedCollection new. "TODO: make linked list?"
    namespace add: primitiveNS; add: globalNS.
    symbolValueStack := LinkedList new.
    self initNativePrimitives.
    emacsServerInterface := EmacsServerInterface new.
	self initServerPrimitives.
	callDepth := 0.
	maxCallDepth := 100.
	timestamp := DateAndTime now.
]

{ #category : #accessing }
EigtContext >> lexicalBinding [
    "TODO: Revise namespace searched."
    ^ (globalNS at: #'lexical-binding') symbolValue ~= self nil.
]

{ #category : #accessing }
EigtContext >> lookup: aSymbol for: aCellTypeSymbol [
    ""
    | foundSymbol |
    (#(any functionCell valueCell) contains: [:ct | ct = aCellTypeSymbol]) 
        ifFalse: [self error: 'aCellTypeSymbol must be #functionCell, #valueCell or #any'].
	namespace reverseDo: [:ns | 
	    foundSymbol := ns at: aSymbol ifAbsent: [nil].
	    foundSymbol ifNotNil: [
	        aCellTypeSymbol = #any ifTrue: [^ foundSymbol].
	        (foundSymbol perform: aCellTypeSymbol) ifNotNil: [^ foundSymbol].
	    ].
	].
    ^ nil.
]

{ #category : #accessing }
EigtContext >> lookupFunctionFor: aSymbol [
    "Search from the most recent local namespace back to global for aSymbol."
    ^ self lookup: aSymbol for: #functionCell.
]

{ #category : #accessing }
EigtContext >> lookupValueFor: aSymbol [
    "Search from the most recent local namespace back to global for aSymbol."
    ^ self lookup: aSymbol for: #valueCell.
]

{ #category : #accessing }
EigtContext >> maxCallDepth [
    ^ maxCallDepth.
]

{ #category : #accessing }
EigtContext >> maxCallDepth: aNumber [
    aNumber isKindOf: Number ifFalse: [self error: 'Maximum call depth must be a number'].
    maxCallDepth := aNumber asInteger.
]

{ #category : #accessing }
EigtContext >> mergeGlobalsWith: anEigtNamespace [
    anEigtNamespace associations do: [:assoc | globalNS at: assoc key put: assoc value].
]

{ #category : #accessing }
EigtContext >> mergeReferencesWith: aDictionary [
    aDictionary associationsDo: [:assoc | refValues at: assoc key put: assoc value]
]

{ #category : #accessing }
EigtContext >> namespace [
    ^ namespace.
]

{ #category : #accessing }
EigtContext >> nil [
    ^ globalNS at: #nil.
]

{ #category : #accessing }
EigtContext >> peekQuote [
    ^ quoteStack first.
]

{ #category : #accessing }
EigtContext >> popQuote [
    ^ quoteStack removeFirst.
]

{ #category : #accessing }
EigtContext >> popSymbolValue [
    | svp |
	self lexicalBinding ifTrue: [^ self].
	svp := symbolValueStack removeFirst.
	svp first set: svp second.
	EigtSignal emit: 'Popped symbol value for: ', svp first print.
]

{ #category : #accessing }
EigtContext >> popSymbolValues: anInteger [
    1 to: anInteger do: [:_ | self popSymbolValue].
]

{ #category : #accessing }
EigtContext >> postCopy [
    self error: 'Context copying is not allowed'.
]

{ #category : #accessing }
EigtContext >> printOn: aStream [
    super printOn: aStream.
    (globalNS isNil or: [symbolValueStack isNil]) ifTrue: [^ self].
    aStream
        nextPutAll: '<id: ', self identityHash asString, '; globals: ', globalNS size asString, '; stack: ', symbolValueStack size asString, '>'.
]

{ #category : #accessing }
EigtContext >> pushQuote: aQuote [
    quoteStack addFirst: aQuote.
]

{ #category : #accessing }
EigtContext >> pushSymbolValue: anEigtSymbol [
    self lexicalBinding ifTrue: [^ self].
    symbolValueStack addFirst: {anEigtSymbol. anEigtSymbol symbolValue}.
    EigtSignal emit: 'Pushed symbol value for: ', anEigtSymbol print.
]

{ #category : #accessing }
EigtContext >> pushSymbolValueFor: anEigtSymbol [
    self lexicalBinding ifTrue: [^ self].
    symbolValueStack addFirst: {anEigtSymbol. anEigtSymbol symbolValue}.
    EigtSignal emit: 'Pushed symbol value for: ', anEigtSymbol print.
]

{ #category : #accessing }
EigtContext >> pushSymbolValues: anEigtSymbolCollection [
    anEigtSymbolCollection do: [:es | self pushSymbolValue: es].
]

{ #category : #accessing }
EigtContext >> pushSymbolValuesFor: anEigtSymbolCollection [
    anEigtSymbolCollection do: [:es | self pushSymbolValueFor: es].
]

{ #category : #accessing }
EigtContext >> quote [
    ^ quoteStack.
]

{ #category : #accessing }
EigtContext >> quote: aQuoteOrFalse [
    ^ quoteStack := aQuoteOrFalse.
]

{ #category : #accessing }
EigtContext >> readWithEngine: anELispString [
    ""
    ^ engine read: anELispString.
]

{ #category : #accessing }
EigtContext >> refs [
    ^ refValues.
]

{ #category : #accessing }
EigtContext >> t [
    ^ globalNS at: #t.
]

{ #category : #accessing }
EigtContext >> tsId [
    ^ timestamp asString.
]
