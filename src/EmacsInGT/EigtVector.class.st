Class {
	#name : #EigtVector,
	#superclass : #EigtArray,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtVector >> initialize [
    super initialize.
    firstChildPos := 2.
]

{ #category : #accessing }
EigtVector >> print [
    ^ '[', (' ' join: (value collect: [:eo | eo print])), ']'.
]

{ #category : #accessing }
EigtVector >> start [
    ^ ast ifNotNil: [ast leftBracket startPosition] ifNil: [start].
]

{ #category : #accessing }
EigtVector >> stop [
    ^ ast ifNotNil: [ast rightBracket stopPosition] ifNil: [stop].
]
