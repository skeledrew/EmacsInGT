Class {
	#name : #'OBSOLETE_BrEditorSelectFromCursorToLineEndShortcut',
	#superclass : #BrEditorShortcut,
	#category : #EmacsInGT
}

{ #category : #accessing }
OBSOLETE_BrEditorSelectFromCursorToLineEndShortcut >> description [
	^ 'Selects from the cursor to the end of the line.'
]

{ #category : #accessing }
OBSOLETE_BrEditorSelectFromCursorToLineEndShortcut >> initialize [
	super initialize.
	
	combination := BlKeyCombination shiftEnd.
]

{ #category : #accessing }
OBSOLETE_BrEditorSelectFromCursorToLineEndShortcut >> name [
	^ 'Select to line end'
]

{ #category : #accessing }
OBSOLETE_BrEditorSelectFromCursorToLineEndShortcut >> performOnEditor: aBrTextEditor element: aBrEditorElement dueTo: aShortcutEvent [
    | selecter startPos text endPos curPos hitNonWS |
    selecter := aBrTextEditor selecter.
    startPos := selecter cursors first position max: 1. "assume a single cursor"
    text := selecter editor text asString.
    startPos >= text size ifTrue: [^ self].
    endPos := DEPRECATED_ShortcutFactory findEndPosIn: text for: '\n|\r' replaceEscapeSequences startingAt: startPos going: #right.
    selecter
		withoutCursorUpdate;
		moveBy: endPos - startPos;
	    select.
	aBrTextEditor navigator
		moveToLineEnd;
		apply
]
