Class {
	#name : #EigtTranspiler,
	#superclass : #ELispExpressionNodeVisitor,
	#instVars : [
		'cachedExprNode',
		'namespace',
		'quoted',
		'inQuote',
		'context'
	],
	#category : #'EmacsInGT-AST'
}

{ #category : #accessing }
EigtTranspiler >> acceptNodes: aCollection [
	^ aCollection collect: [ :each | self acceptNode: each ].
]

{ #category : #accessing }
EigtTranspiler >> context [
    ^ EigtEngine getContextAt: context.
]

{ #category : #accessing }
EigtTranspiler >> context: anEigtContext [
    context := anEigtContext.
]

{ #category : #accessing }
EigtTranspiler >> eval [
    "Evaluate a cached node.
    
    NOTE: This is a convenience method to enable chaining from #read:.
    "
    | result |
	result := self eval: nil.
	cachedExprNode := nil.
	^ result.
]

{ #category : #accessing }
EigtTranspiler >> eval: anELispExpressionNode [
    "Evaluate anELispExpressionNode and return the result."
    | sexp |
    anELispExpressionNode isNil
        ifTrue: [
            cachedExprNode isNil ifTrue: [self error: 'No expression node provided.'].
        sexp := cachedExprNode.
        ]
        ifFalse: [sexp := anELispExpressionNode].
    ^ self acceptNode: sexp.
]

{ #category : #accessing }
EigtTranspiler >> initialize [
    namespace := EigtNamespace new.
]

{ #category : #accessing }
EigtTranspiler >> read: aString [
    "Read aString and convert to anELispExpressionNode or anELispObject."
    ^ cachedExprNode := ELispParser parse: aString.
]

{ #category : #accessing }
EigtTranspiler >> visitCharacter: aCharacter [
    "Store and behave as integer, show as char"
    ^ EigtCharacter ast: aCharacter.
]

{ #category : #accessing }
EigtTranspiler >> visitCons: aCons [
    | inst value |
    inst := EigtCons ast: aCons.
	value := self visitExpression: aCons. "NOTE: do this here since there are sub-nodes"
	inst
	    context: context;
	    start: aCons leftParen startPosition;
	    stop: aCons rightParen stopPosition.
	value do: [:eo | eo parent: inst].
	((aCons respondsTo: #dot) and: [aCons dot value = '.'])
	    ifTrue: [inst cons: value first to: value second; yourself]
	    ifFalse: [inst value: value].
	^ inst.
]

{ #category : #accessing }
EigtTranspiler >> visitFile: aFile [
    | inst value |
	value := self visitExpression: aFile. "NOTE: do this here since there are sub-nodes"
	^ EigtFile initWithAst: aFile andContext: context andValue: value.
]

{ #category : #accessing }
EigtTranspiler >> visitFloat: aFloat [
    ^ EigtFloat ast: aFloat.
]

{ #category : #accessing }
EigtTranspiler >> visitFunction: aFunction [
    | inst value |
    inst := EigtFn ast: aFunction.
	value := self visitExpression: aFunction.
	inst value: value.
	inst quote: aFunction hashQuote value;
	    context: context;
	    start: aFunction hashQuote startPosition;
	    stop: value last stop.
	value first parent: inst.
	^ inst.
]

{ #category : #accessing }
EigtTranspiler >> visitInteger: anInteger [
    ^ EigtInteger ast: anInteger.
]

{ #category : #accessing }
EigtTranspiler >> visitQuote: aQuote [
    "Handle the quote and backquote shortcuts."
    | value inst |
    value := self visitExpression: aQuote.
    inst := EigtQuote initWithAst: aQuote andContext: context andValue: value first.
    value first parent: inst.
	inst
	    quote: aQuote quote value;
	    start: aQuote quote startPosition;
	    stop: aQuote quote stopPosition.
    ^ inst.
]

{ #category : #accessing }
EigtTranspiler >> visitRecord: aRecordOrHashTable [
    | inst value |
	value := self visitExpression: aRecordOrHashTable.
	inst := (value first name = 'hash-table' ifTrue: [EigtHashTable] ifFalse: [EigtRecord]) ast: aRecordOrHashTable.
	value do: [:eo | eo parent: inst].
	inst value: value.
	^ inst.
]

{ #category : #accessing }
EigtTranspiler >> visitSmaCCParseNode: aSmaCCParseNode [
	"
	
	NOTE: We do infinite recursion protection here as it seems to be a hotspot.
	"
	| result |
	self context increaseCallDepth.
	result := self acceptNodes: aSmaCCParseNode sortedChildren.
	self context decreaseCallDepth.
	^ result
]

{ #category : #accessing }
EigtTranspiler >> visitString: aString [
    ^ EigtString ast: aString.
]

{ #category : #accessing }
EigtTranspiler >> visitSymbol: aSymbol [
    | inst |
	inst := EigtSymbol findOrCreateFromAst: aSymbol withContext: context.
	inst quote: self context quote.
	inst start: aSymbol name startPosition.
	^ inst.
]

{ #category : #accessing }
EigtTranspiler >> visitUnquote: anUnquote [
    "Handle the comma and splice shortcuts."
    | value inst |
    value := self visitExpression: anUnquote.
    inst := EigtUnquote initWithAst: anUnquote andContext: context andValue: value first.
    value first parent: inst.
    inst
        quote: anUnquote unquote value;
        start: anUnquote unquote startPosition;
        stop: anUnquote unquote stopPosition.
    ^ inst.
]

{ #category : #accessing }
EigtTranspiler >> visitVector: aVector [
    | inst value |
	inst := EigtVector ast: aVector.
	value := self visitExpression: aVector.
	value do: [:eo | eo parent: inst].
	inst
	    context: context;
	    value: value;
	    start: aVector leftBracket startPosition;
	    stop: aVector rightBracket stopPosition.
	^ inst.
]
