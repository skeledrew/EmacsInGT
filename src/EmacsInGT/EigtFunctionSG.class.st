Class {
	#name : #EigtFunctionSG,
	#superclass : #EigtSubrGroup,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtFunctionSG class >> apply: anEigtArgsList [
    <eigtSubr: 'apply'>
    | args |
	anEigtArgsList get last class = EigtCons ifFalse: [self error: 'Wrong type of argument: listp, ', anEigtArgsList get last print].
    args := anEigtArgsList get allButLast asOrderedCollection.
    args addAll: anEigtArgsList get last value.
    ^ self funcall: (EigtArgsList new put: args; context: anEigtArgsList context; yourself).
]

{ #category : #accessing }
EigtFunctionSG class >> closure: anEigtArgsList [
    <eigtSubr: 'closure'>
    ^ EigtCons new
        context: anEigtArgsList context;
        value: (anEigtArgsList get addFirst: (EigtSymbol findOrCreateWithContext: anEigtArgsList context andName: 'closure'); yourself);
        parent: false;  "TODO: should prob have the same parent as it's origin"
        yourself.
]

{ #category : #accessing }
EigtFunctionSG class >> defalias: anEigtArgsList [
    "Ref: https://www.gnu.org/software/emacs/manual/html_node/elisp/Defining-Functions.html
    
    TODO: Account for `defalias-fset-function` property.
        - Handle doc.
    "
    <eigtSubr: 'defalias'>
    anEigtArgsList first fset: anEigtArgsList second.
    ^ anEigtArgsList context t.
]

{ #category : #accessing }
EigtFunctionSG class >> eval: anEigtArgsList [
    "TODO: Add optional lexical handling"
    <eigtSubr: 'eval'>
    | eo |
	eo := anEigtArgsList first.
    ^ eo context evalWithEngine: eo.
]

{ #category : #accessing }
EigtFunctionSG class >> fboundp: anEigtArgsList [
    <eigtSubr: 'fboundp'>
    ^ anEigtArgsList first symbolFunction
        ifNil: [anEigtArgsList context nil]
        ifNotNil: [anEigtArgsList context t].
]

{ #category : #accessing }
EigtFunctionSG class >> funcall: anEigtArgsList [
    <eigtSubr: 'funcall'>
    | args function context pos opt rest funcArgs result newArgs |
    args := anEigtArgsList get.
	function := args first.
	function class = EigtLambda ifTrue: [function := function value.].
	(function isEigtLambda or: [function isEigtClosure]) ifTrue: [
	    funcArgs := EigtArgsList new put: args allButFirst; context: anEigtArgsList context; yourself.
	    funcArgs setVars: function cdr car.
	    (function value allButFirst: 2) do: [:form | result := form eval].  "eval the lambda forms"
	    funcArgs unsetVars.
	] ifFalse: [
	    "TODO: Signal on macro or special form."
	    newArgs := OrderedCollection new "no need to quote the function"
	        add: args first;
	        addAll: (args allButFirst collect: [:arg | EigtQuote new value: arg]);
            yourself.
	    result := EigtCons new 
	        context: anEigtArgsList context;
	        value: newArgs;
	        parent: false;
	        eval.
	].
	^ result.
]

{ #category : #accessing }
EigtFunctionSG class >> function: anEigtArgsList [
    "Ref: https://www.gnu.org/software/emacs/manual/html_node/elisp/Anonymous-Functions.html
    "
    <eigtSubr: 'function'>
    ^ self makeClosure: anEigtArgsList first.
]

{ #category : #accessing }
EigtFunctionSG class >> lambda: anEigtArgsList [
    "NOTE: We replace the car which got removed due to function eval, so index refs line up.
    TODO: Replace with macro.
    "
    <eigtSubr: 'lambda'>
    | expr |
	expr := EigtCons new
        context: anEigtArgsList context;
        parent: anEigtArgsList first parent;
        value: (anEigtArgsList get addFirst: (EigtSymbol findOrCreateWithContext: anEigtArgsList context andName: 'lambda'); yourself);
        yourself.
    ^ self makeClosure: expr.
]

{ #category : #accessing }
EigtFunctionSG class >> makeClosure: anEigtLambda [
    "Create a closure from a lambda if lexical binding is active.
    
    TODO: Ensure lexical data is preserved.
    "
    | value cons |
    ^ anEigtLambda context lexicalBinding
        ifTrue: [
            value := anEigtLambda value.
            value at: 1 put: (EigtSymbol findOrCreateWithContext: anEigtLambda context andName: 'closure').
            cons := EigtCons withContext: anEigtLambda andValue: value.
            cons parent: anEigtLambda parent.
            cons.
        ]
        ifFalse: [anEigtLambda].
]
