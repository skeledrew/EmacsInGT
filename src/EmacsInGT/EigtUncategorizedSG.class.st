Class {
	#name : #EigtUncategorizedSG,
	#superclass : #EigtSubrGroup,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtUncategorizedSG class >> and: anEigtArgsList [
    <eigtSubr: 'and'>
    | result |
	result := anEigtArgsList context t.
    anEigtArgsList get do: [:arg |
        result := arg context: anEigtArgsList context; eval.
        result = anEigtArgsList context nil ifTrue: [^ result].
    ].
    ^ result.
]

{ #category : #accessing }
EigtUncategorizedSG class >> catch: anEigtArgsList [
    <eigtSubr: ''>
    self notYetImplemented
]

{ #category : #accessing }
EigtUncategorizedSG class >> cond: anEigtArgsList [
    <eigtSubr: 'cond'>
    | args result |
	args := anEigtArgsList get.
	result := anEigtArgsList context nil.
	args do: [:clause |
	    result := clause car context: anEigtArgsList context; eval.
	    result ~= anEigtArgsList context nil ifTrue: [
	        clause cdr value ifEmpty: [^ result].
	        clause cdr value do: [:form | result := form context: anEigtArgsList context; eval].
	        ^ result.
	    ]
	].
	^ result.
]

{ #category : #accessing }
EigtUncategorizedSG class >> conditionCase: anEigtArgsList [
    <eigtSubr: 'condition-case'>
    self notYetImplemented
]

{ #category : #accessing }
EigtUncategorizedSG class >> defconst: anEigtArgsList [
    <eigtSubr: 'defconst'>
    | args context |
	args := anEigtArgsList get.
	context := anEigtArgsList context.
	args first isSpecial: true.
    args first set: (args second context: context; eval).
    args first put: context t in: (EigtSymbol new withName: #'risky-local-variable' andContext: context).
    args size = 3 ifTrue: [args first put: args third in: (EigtSymbol new withName: #'variable-documentation' andContext: context)].
    ^ args first.
]

{ #category : #accessing }
EigtUncategorizedSG class >> defmacro: anEigtArgsList [
    <eigtSubr: ''>
    self notYetImplemented
]

{ #category : #accessing }
EigtUncategorizedSG class >> defvar: anEigtArgsList [
    <eigtSubr: 'defvar'>
    | args context |
	args := anEigtArgsList get.
	context := anEigtArgsList context.
    (args first symbolValue = nil and: [args size >= 2]) ifTrue: [
        args first set: (args second context: context; eval).
        args first isSpecial: true.
        args size = 3 ifTrue: [args first put: args third in: (EigtSymbol new withName: #'variable-documentation' andContext: context)].
    ].
    ^ args first.
]

{ #category : #accessing }
EigtUncategorizedSG class >> eq: anEigtArgsList [
    <eigtSubr: 'eq'>
    ^ anEigtArgsList first = anEigtArgsList second
        ifTrue: [anEigtArgsList context t]
        ifFalse: [anEigtArgsList context nil].
]

{ #category : #accessing }
EigtUncategorizedSG class >> errorM: anEigtArgsList [
    <eigtSubr: 'error'>
    | text |
	text := (self call: 'format-message' with: anEigtArgsList) print.
	self error: text.
]

{ #category : #accessing }
EigtUncategorizedSG class >> errorMessage: anEigtArgsList [
    <eigtSubr: 'error'>
    | text |
	text := (self call: 'format-message' with: anEigtArgsList) print.
	self error: text.
]

{ #category : #accessing }
EigtUncategorizedSG class >> evalAndCompile: anEigtArgsList [
    <eigtSubr: 'eval-and-compile'>
    ^ self call: 'progn' with: anEigtArgsList.
]

{ #category : #accessing }
EigtUncategorizedSG class >> formatMessage: anEigtArgsList [
    <eigtSubr: 'format-message'>
    | text |
	text := anEigtArgsList first print.
    ^ EigtString initWithContext: anEigtArgsList andValue: text.
]

{ #category : #accessing }
EigtUncategorizedSG class >> fset: anEigtArgsList [
    <eigtSubr: 'fset'>
    ^ anEigtArgsList first fset: anEigtArgsList get second.
]

{ #category : #accessing }
EigtUncategorizedSG class >> get: anEigtArgsList [
    <eigtSubr: 'get'>
    ^ anEigtArgsList first get: anEigtArgsList second.
]

{ #category : #accessing }
EigtUncategorizedSG class >> if: anEigtArgsList [
    <eigtSubr: 'if'>
    | args result |
	args := anEigtArgsList get.
	args first eval ~= anEigtArgsList context nil
	    ifTrue: [^ args second eval].
	args size = 2 ifTrue: [^ anEigtArgsList context nil].
	3 to: args size do: [:idx | result := (args at: idx) eval].
	^ result.
]

{ #category : #accessing }
EigtUncategorizedSG class >> increment: anEigtArgsList [
    <eigtSubr: '1+'>
    ^ EigtInteger value: anEigtArgsList first value + 1.
]

{ #category : #accessing }
EigtUncategorizedSG class >> interactive: anEigtArgsList [
    <eigtSubr: ''>
    self notYetImplemented
]

{ #category : #accessing }
EigtUncategorizedSG class >> intern: anEigtArgsList [
    <eigtSubr: 'intern'>
    ^ EigtSymbol findOrCreateWithContext: anEigtArgsList andName: anEigtArgsList first value.
]

{ #category : #accessing }
EigtUncategorizedSG class >> let: anEigtArgsList [
    <eigtSubr: 'let'>
    | context bindings numBinds result bindValues bindVariables |
	context := anEigtArgsList context.
	bindings := anEigtArgsList first.
	numBinds := 0.
	result := context nil.
	bindVariables := Array new: bindings value size.
	bindValues := Array new: bindings value size.
	bindings value do: [:binding |
	    binding isEigtSymbol 
	        ifTrue: [context pushSymbolValue: binding] 
	        ifFalse: [context pushSymbolValue: binding car].
	    numBinds := numBinds + 1.
	    binding isEigtSymbol ifTrue: [
	        bindVariables at: numBinds put: binding.
	        bindValues at: numBinds put: context nil.
	    ].
	    binding car isEigtSymbol & (binding value size = 1)
	        ifTrue: [
	            bindVariables at: numBinds put: binding car.
	            bindValues at: numBinds put: context nil.
	        ]
	        ifFalse: [
	            bindVariables at: numBinds put: binding car.
	            bindValues at: numBinds put: binding cdr car eval.
	        ].
	    "NOTE: Use of incompatible types is implicitly caught via `car`+`&`."
	].
	1 to: numBinds do: [:idx | (bindVariables at: idx) lexicalSet: (bindValues at: idx)].
	anEigtArgsList get allButFirst do: [:form | 
	    result := form eval
	].
	context popSymbolValues: numBinds.
	^ result.
]

{ #category : #accessing }
EigtUncategorizedSG class >> letStar: anEigtArgsList [
    <eigtSubr: 'let*'>
    | context bindings numBinds result |
	context := anEigtArgsList context.
	bindings := anEigtArgsList first.
	numBinds := 0.
	result := context nil.
	bindings value do: [:binding |
	    binding isEigtSymbol 
	        ifTrue: [context pushSymbolValue: binding] 
	        ifFalse: [context pushSymbolValue: binding car].
	    binding isEigtSymbol ifTrue: [binding lexicalSet: context nil].
	    binding car isEigtSymbol & (binding value size = 1)
	        ifTrue: [binding car lexicalSet: context nil]
	        ifFalse: [binding car lexicalSet: binding cdr car eval].
	    "NOTE: Use of incompatible types is implicitly caught via `car`+`&`."
	    numBinds := numBinds + 1.
	].
	anEigtArgsList get allButFirst do: [:form | 
	    result := form eval
	].
	context popSymbolValues: numBinds.
	^ result.
]

{ #category : #accessing }
EigtUncategorizedSG class >> message: anEigtArgsList [
    <eigtSubr: 'message'>
    | anEigtString |
	anEigtString := self call: 'format-message' with: anEigtArgsList.
    BlBlocUIManagerNotification
        requestOrNil: #inform:
        with: { anEigtString print }.
    ^ anEigtString.
]

{ #category : #accessing }
EigtUncategorizedSG class >> null: anEigtArgsList [
    <eigtSubr: 'null'>
    ^ anEigtArgsList first = anEigtArgsList context nil
        ifTrue: [anEigtArgsList context t]
        ifFalse: [anEigtArgsList context nil].
]

{ #category : #accessing }
EigtUncategorizedSG class >> or: anEigtArgsList [
    <eigtSubr: 'or'>
    | result |
	result := anEigtArgsList context nil.
    anEigtArgsList get do: [:arg |
        result := arg context: anEigtArgsList context; eval.
        result ~= anEigtArgsList context nil ifTrue: [^ result].
    ].
    ^ result.
]

{ #category : #accessing }
EigtUncategorizedSG class >> prog1: anEigtArgsList [
    <eigtSubr: 'prog1'>
    anEigtArgsList get size = 0 ifTrue: [self error: 'Wrong number of arguments: prog1, ', anEigtArgsList get size asString].
    ^ (anEigtArgsList get collect: [:arg | arg eval]) first.
]

{ #category : #accessing }
EigtUncategorizedSG class >> prog2: anEigtArgsList [
    <eigtSubr: 'prog2'>
    anEigtArgsList get size < 2 ifTrue: [self error: 'Wrong number of arguments: prog2, ', anEigtArgsList get size asString].
    ^ (anEigtArgsList get collect: [:arg | arg eval]) second.
]

{ #category : #accessing }
EigtUncategorizedSG class >> progn: anEigtArgsList [
    <eigtSubr: 'progn'>
    ^ (anEigtArgsList get collect: [:arg | arg eval])
        ifEmpty: [anEigtArgsList context nil]
        ifNotEmpty: [:results | results last].
]

{ #category : #accessing }
EigtUncategorizedSG class >> put: anEigtArgsList [
    <eigtSubr: 'put'>
    anEigtArgsList first put: anEigtArgsList third in: anEigtArgsList second.
    ^ anEigtArgsList third.
]

{ #category : #accessing }
EigtUncategorizedSG class >> saveCurrentBuffer: anEigtArgsList [
    <eigtSubr: 'save-current-buffer'>
    self notYetImplemented
]

{ #category : #accessing }
EigtUncategorizedSG class >> saveExcursion: anEigtArgsList [
    <eigtSubr: 'save-excursion'>
    self notYetImplemented
]

{ #category : #accessing }
EigtUncategorizedSG class >> saveRestriction: anEigtArgsList [
    <eigtSubr: 'save-restriction'>
    self notYetImplemented
]

{ #category : #accessing }
EigtUncategorizedSG class >> set: aList [
    <eigtSubr: 'set'>
    ^ aList first set: aList second.  "TODO: verify if we should use a particular namespace."
]

{ #category : #accessing }
EigtUncategorizedSG class >> setq: aList [
    "TODO: validate args"
    <eigtSubr: 'setq'>
    | result args |
    args := aList class = EigtArgsList ifTrue: [aList get] ifFalse: [aList].
	result := self set: {args first. args second eval}.
	args size > 2
	    ifTrue: [^ self setq: (args allButFirst: 2)]
	    ifFalse: [^ result].
]

{ #category : #accessing }
EigtUncategorizedSG class >> setqDefault: anEigtArgsList [
    <eigtSubr: 'setq-default'>
    self notYetImplemented
]

{ #category : #accessing }
EigtUncategorizedSG class >> stringP: anEigtArgsList [
    <eigtSubr: 'stringp'>
    ^ anEigtArgsList first class = EigtString
        ifTrue: [anEigtArgsList context t]
        ifFalse: [anEigtArgsList context nil].
]

{ #category : #accessing }
EigtUncategorizedSG class >> symbolFunction: anEigtArgsList [
    ""
    <eigtSubr: 'symbol-function'>
    ^ anEigtArgsList first symbolFunction.
]

{ #category : #accessing }
EigtUncategorizedSG class >> unwindProtect: anEigtArgsList [
    <eigtSubr: 'unwind-protect'>
    self notYetImplemented
]

{ #category : #accessing }
EigtUncategorizedSG class >> while: anEigtArgsList [
    <eigtSubr: 'while'>
    anEigtArgsList get size = 0 ifTrue: [self error: 'Wrong number of arguments: while, 0'].
    [anEigtArgsList first eval ~= anEigtArgsList context nil]
        whileTrue: [anEigtArgsList get allButFirst do:[:form | form eval]].
    ^ anEigtArgsList context nil.
]
