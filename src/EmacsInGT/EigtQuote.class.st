"
I am a convenience class to ease the nesting of quote expressions and backquote processing. Not sure if I should be in the core...
"
Class {
	#name : #EigtQuote,
	#superclass : #EigtCons,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtQuote class >> withContext: anEigtContext andQuote: aString andValue: anEigtObject [
    self notYetImplemented
]

{ #category : #accessing }
EigtQuote >> children [
    ^ {self value}.
]

{ #category : #accessing }
EigtQuote >> eval [
    "NOTE: The quote value is either apostrophe or backquote."
    ^ (quote = '`')
        ifFalse: [value context: context; yourself]
        ifTrue: [
            self context pushQuote: quote.
            self evalBackquote: value.
        ].
]

{ #category : #accessing }
EigtQuote >> evalBackquote: anEigtObject [
    "Evaluate a backquoted cons.
    
    NOTE: This implementation creates a new container cons.
    "
    | result |
    anEigtObject class ~= EigtCons ifTrue: [^ anEigtObject].
    result := OrderedCollection new.
	anEigtObject value do: [:ele |
        ele class = EigtUnquote ifTrue: [result add: ele eval].
        ele class = EigtCons ifTrue: [result add: (self evalBackquote: ele)].
        ({EigtUnquote . EigtCons} allSatisfy: [:cls | ele class ~= cls]) ifTrue: [result add: ele].
    ].
    ^ EigtCons withContext: context andValue: result flattened.
]

{ #category : #accessing }
EigtQuote >> initialize [
    super initialize.
    firstChildPos := 3.
]

{ #category : #accessing }
EigtQuote >> print [
    ^ (quote ifNil: ''''), self value print.
]

{ #category : #accessing }
EigtQuote >> value [
    ^ value.
]

{ #category : #accessing }
EigtQuote >> value: anObject [
    "Store a value who's type is representative of the object's class."
    value = nil
        ifTrue: [value := anObject]
        ifFalse: [self error: 'Value is write once.'].
]
