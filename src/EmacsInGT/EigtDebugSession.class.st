Class {
	#name : #EigtDebugSession,
	#superclass : #GeaAbstractDebugSession,
	#instVars : [
		'sessionName',
		'context',
		'process',
		'__session',
		'ctxVars'
	],
	#category : #'EmacsInGT-Utilities'
}

{ #category : #accessing }
EigtDebugSession class >> named: aString on: anEigtDebugProcess startedAt: aContext [
    ^ self new
        sessionName: aString;
        process: anEigtDebugProcess;
        context: aContext;
        yourself.
]

{ #category : #accessing }
EigtDebugSession >> context: anEigtDebugContext [
    context := anEigtDebugContext.
]

{ #category : #accessing }
EigtDebugSession >> contextVariables [
    "NOTE: See setter..."
    ^ GeaTracePointVariables from: ("context expr context copyGlobals" ctxVars associations collect: [:assoc | assoc value symbolName->assoc value symbolValue]) asDictionary.
]

{ #category : #accessing }
EigtDebugSession >> contextVariables: anEigtNamespace [
    "NOTE: We need a setter while using the records creating generator in process, else we would be getting the same variable set for all evaluated expressions."
    ctxVars := anEigtNamespace.
]

{ #category : #accessing }
EigtDebugSession >> process: anEigtDebugProcess [
    process := anEigtDebugProcess.
]

{ #category : #accessing }
EigtDebugSession >> sessionName: aString [
    sessionName := aString.
]

{ #category : #accessing }
EigtDebugSession >> stepInto [
    ^ process debugActionDo: #stepInto
]

{ #category : #accessing }
EigtDebugSession >> stepOver [
    ^ process debugActionDo: #stepOver
]
