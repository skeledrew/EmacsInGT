"
Interface with a running Emacs server via emacsclient or TCP.
"
Class {
	#name : #EmacsServerInterface,
	#superclass : #Object,
	#instVars : [
		'host',
		'port',
		'clientPath',
		'runStats'
	],
	#category : #'EmacsInGT-Utilities'
}

{ #category : #accessing }
EmacsServerInterface >> clientPath: aString [
    clientPath := aString.
]

{ #category : #accessing }
EmacsServerInterface >> eval: anELispString [
    | then function stat res |
    function := anELispString first = $(
        ifTrue: [(anELispString allButFirst allButLast findTokens:' ') first]
        ifFalse: ['[N/A]'].
    stat := runStats at: function ifAbsentPut: [{0 . DateAndTime now - DateAndTime now}].
	then := DateAndTime now.
    res := self evalViaClient: anELispString.
    stat at: 1 put: ((stat at: 1) + 1).  "run count"
    stat at: 2 put: ((stat at: 2) + (DateAndTime now - then)).  "total run time"
    ^ res.
]

{ #category : #accessing }
EmacsServerInterface >> evalViaClient: anELispString [
    "Evaluate anELispString by passing to emacsclient.
    
    NOTE: We trim spaces to get rid of the ending newline.
          Using `OSSUnixSubprocess` instead of `LibC` as it properly handles the argument.
    "
    | result |
	OSSUnixSubprocess new
	    command: clientPath;
	    arguments: {'-e'. anELispString};
	    redirectStdout;
	    runAndWaitOnExitDo: [ :process :outString  |
		    result := outString.
        ].
     ^ result trim.
]

{ #category : #accessing }
EmacsServerInterface >> gtRunStatsFor: aView [
	<gtView>
	^ aView columnedTree
		title: 'Server Stats';
		priority: 1;
		items: [ runStats associations ];
		children: [ :each | 
			each value isDictionary
				ifTrue: [ each value associations ]
				ifFalse: [ (each value isArray and: [ each value allSatisfy: #isDictionary ])
						ifTrue: [ each value collectWithIndex: [ :x :i | i -> x ] ]
						ifFalse: [ #() ] ] ];
		column: 'Function' text: [ :assoc | assoc key ];
		column: 'Statistics' text: [ :assoc | self makeRunStat: assoc value ];
		send: [ :assoc | assoc value ]
]

{ #category : #accessing }
EmacsServerInterface >> initialize [
    clientPath := Eigtils loadSimpleKVData at: 'emacsclient'.
    runStats := Dictionary new.
]

{ #category : #accessing }
EmacsServerInterface >> makeRunStat: aCollection [
    ""
    ^ 'ran ', (aCollection at: 1) asString, 'x, for ', (aCollection at: 2) asSeconds asString, ' seconds'.
]
