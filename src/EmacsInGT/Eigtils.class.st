"
I contain utility methods to primarily aid in development, and so may be removed in the future.
"
Class {
	#name : #Eigtils,
	#superclass : #Object,
	#category : #'EmacsInGT-Utilities'
}

{ #category : #accessing }
Eigtils class >> cond: aCollection [
    "Perform Lisp-style conditional execution."
    aCollection last first = true ifFalse: [self error: 'No default (true-condition) clause found in cond.'].
    aCollection do: [:clause | clause first value ifTrue: [^ clause second value]].
    self error: 'Something seems wrong. We shouldn''t be here...'.
]

{ #category : #accessing }
Eigtils class >> inFuelFile: aFuelPath atHeaderEntry: aString [
    ^ (FLMaterializer materializeHeaderFromFileNamed: aFuelPath)
        additionalObjectAt: aString asSymbol.
]

{ #category : #accessing }
Eigtils class >> inFuelFile: aFuelPath atHeaderEntry: anEntrySymbol put: anEntryObject [
    "Write extra metadata to a fuel file.
    
    NOTE: Will be very slow if the file is too large.
    TODO: Add check for files over some limit.
    "
    | oldObject oldMeta serializer |
	oldObject := FLMaterializer materializeFromFileNamed: aFuelPath.
    oldMeta := (FLMaterializer materializeHeaderFromFileNamed: aFuelPath) eigtGetMetaData.
    (oldMeta at: #extra) at: anEntrySymbol put: anEntryObject.
    serializer := FLSerializer newDefault.
    serializer header eigtSetMetaData: oldMeta.
    serializer serialize: oldObject toFileNamed: aFuelPath.
]

{ #category : #accessing }
Eigtils class >> loadFuelFile: aFuelPath [
    ^ FLMaterializer materializeFromFileNamed: aFuelPath.
]

{ #category : #accessing }
Eigtils class >> loadSimpleKVData [
    "Load a simple space-separated key-value datafile."
    | data dataDict kv |
    data := 'pharo-local/data.txt' asFileReference contents findTokens: Character lf asString.
    dataDict := Dictionary new.
    data do: [:line | kv := line findTokens: ' '. dataDict at: kv first put: (' ' join: kv allButFirst)].
    ^ dataDict.
]

{ #category : #accessing }
Eigtils class >> makeSubrTemplates: aCollection [
    ""
    | tmpl sGClasses |
	tmpl := '_NAME_: anEigtArgsList
    <eigtSubr: ''''>'.
    sGClasses := 'EmacsInGT' asPackage classes select: [:cls | cls name endsWith: 'SG'].
    aCollection do: [:n |
        (sGClasses anySatisfy: [:c | c respondsTo: (n, ':') asSymbol]) ifFalse:
            [EigtUncategorizedSG class compile: ((tmpl copyReplaceAll: '_NAME_' with: n))]].
]

{ #category : #accessing }
Eigtils class >> moveSubr: aSubrName to: aClassMatcher [
    "Move a subr method from uncategorized to the given class matcher."
    | class |
        class := ('Eigt', aClassMatcher, 'SG') asClass.
    class class compile: (EigtUncategorizedSG class >> ((aSubrName, ':') asSymbol)) sourceCode.
    EigtUncategorizedSG class methodDict removeKey: aSubrName asSymbol ifAbsent: [false].
]

{ #category : #accessing }
Eigtils class >> runAfterEvalBlockIn: meta on: anEigtObject [
    ""
    (meta at: #afterEvalBlock ifAbsent: [[:eo | true]]) value: anEigtObject.
]

{ #category : #accessing }
Eigtils class >> runBeforeEvalBlockIn: meta on: anEigtObject [
    ""
    (meta at: #beforeEvalBlock ifAbsent: [[:eo | true]]) value: anEigtObject.
]

{ #category : #accessing }
Eigtils class >> save: anObject toFuelFile: aFuelPath [
    ^ FLSerializer serialize: anObject toFileNamed: aFuelPath.
]

{ #category : #accessing }
Eigtils class >> wrapStObject: anObject [
    "Wrap a native object into the corresponding EigtObject."
    self cond:
        {
            {[anObject isKindOf: Symbol]. [^ EigtSymbol value: anObject]}.
            {[anObject isKindOf: String]. [^ EigtString value: anObject]}.
            {[anObject isKindOf: Integer]. [^ EigtInteger value: anObject]}.
            {[anObject isKindOf: Float]. [^ EigtFloat value: anObject]}.
            {[anObject isKindOf: String]. [^ EigtString value: anObject]}.
            {[anObject isKindOf: String]. [^ EigtString value: anObject]}.
            {[anObject isKindOf: String]. [^ EigtString value: anObject]}.
            {[anObject isKindOf: String]. [^ EigtString value: anObject]}.
            {true. nil}
        } .
    self error: 'Unable to wrap unrecognized object: ', anObject class asString.
]
