Class {
	#name : #EigtComment,
	#superclass : #Object,
	#instVars : [
		'fileLocalVariables',
		'content'
	],
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtComment >> initialize [
    fileLocalVariables := Dictionary new.
]

{ #category : #accessing }
EigtComment >> process: aString [
    ""
    | lines |
	lines := aString findTokens: String cr, String lf.
	
]
