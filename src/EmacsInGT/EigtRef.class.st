"
I am a value reference, primarily to help multiple instances of a symbol properly reference the same value.
"
Class {
	#name : #EigtRef,
	#superclass : #Object,
	#instVars : [
		'context',
		'refName',
		'value'
	],
	#category : #'EmacsInGT-Utilities'
}

{ #category : #accessing }
EigtRef class >> initWithContext: anEigtContext andName: aString andValue: anObject [
    ^ self new
        context: anEigtContext;
        at: aString put: anObject;
        yourself.
]

{ #category : #accessing }
EigtRef >> at: aName put: anObject [
    refName ifNotNil: [self error: 'Changing the name of a reference is not allowed'].
    anObject class = self class ifTrue: [Halt now].
    refName := aName.
    self value: anObject.
]

{ #category : #accessing }
EigtRef >> context [
    ^ EigtEngine getContextAt: context.
]

{ #category : #accessing }
EigtRef >> context: anEigtContext [
    | ctx |
	ctx := anEigtContext isNumber
        ifTrue: [anEigtContext]
        ifFalse: [anEigtContext identityHash].
    anEigtContext asString = 'nil' ifTrue: [self error: 'Context not initialized'].
    (context asString ~= 'nil' and: [context ~= ctx]) ifTrue: [
    EigtSignal emit: 'Changing context: ', refName asString, '; cid: ', context "identityHash" asString, ' -> ', ctx "anEigtContext identityHash" asString.
    [(EigtEngine getContextAt: ctx) refs at: refName put: (self value)] on: KeyNotFound do: [nil]. "FIXME: This feels like a hack around a deeper issue."
    ].
    context := ctx.
]

{ #category : #accessing }
EigtRef >> doesNotUnderstand: aMessage [
    "Used so we don't need to explicitly update everywhere it turns up.
    
    NOTE: The DNU may inadvertedly mask some areas with implemented methods intended for the referenced value.
    "
    ^ self value
        perform: aMessage selector
        withArguments: aMessage arguments.
]

{ #category : #accessing }
EigtRef >> gtWrappedFor: aView [
    <gtView>
    refName ifNil: [^ aView empty].
    ^ aView columnedList
        title: 'Wrapped';
        priority: 5;
        items: {
            {'Reference Name' . refName asString}.
            {'Value' . self value}
        };
        column: 'Field' text: [:item | item first];
        column: 'Value' text: [:item | item second];
        send: #second.
]

{ #category : #accessing }
EigtRef >> printOn: aStream [
    aStream
        nextPutAll: self value asString;
        nextPutAll: ' via '.
    super printOn: aStream.
    aStream
        nextPut: $<;
        nextPutAll: (self identityHash asString);
        nextPut: $>
]

{ #category : #accessing }
EigtRef >> value [
    "^ self context refs at: refName ifAbsent: [nil]."
    ^ value.
]

{ #category : #accessing }
EigtRef >> value: anObject [
    anObject class = self class ifTrue: [Halt now].  "No nesting"
    "self context refs at: refName put: anObject."
    (([anObject = self context nil and: [refName beginsWith: 'EigtSymbol::nil::']] on: Error do: [false])) ifTrue: [^ self].  "NOTE: Prevent a strange case of infinite recursion"
    value := anObject.
]
