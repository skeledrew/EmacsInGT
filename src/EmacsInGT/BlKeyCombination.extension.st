Extension { #name : #BlKeyCombination }

{ #category : #'*EmacsInGT' }
BlKeyCombination class >> shiftEnd [
    ^ self builder shift end build
]

{ #category : #'*EmacsInGT' }
BlKeyCombination class >> shiftHome [
    ^ self builder shift home build
]

{ #category : #'*EmacsInGT' }
BlKeyCombination class >> shiftPrimaryArrowLeft [
    ^ self builder shift primary arrowLeft build
]

{ #category : #'*EmacsInGT' }
BlKeyCombination class >> shiftPrimaryArrowRight [
    ^ self builder shift primary arrowRight build
]
