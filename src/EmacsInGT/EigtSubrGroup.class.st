"
Primitive operations.

NOTE ^1: There is a strange check in `Object >> #perform:withArguments:inSuperClass:` for `selector numArgs = argArray size`. This breaks the overloading of non-alphabetic symbols accepting 2+ size collections. It is bypassed by wrapping in another collection
"
Class {
	#name : #EigtSubrGroup,
	#superclass : #Object,
	#instVars : [
		'primitives',
		'meta',
		'context'
	],
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtSubrGroup class >> _doesNotUnderstand: aMessage [
    | coll binaryOperators |
    binaryOperators := '[~\-!@%&*+=\\|?/><,]+'.
    (aMessage selector asString matchesRegex: binaryOperators)
        ifTrue: [
            coll := OrderedCollection new add: aMessage selector; addAll: aMessage arguments first "NOTE: ^1"; yourself.
            ^ self doNativeBinaryOperation: coll].
]

{ #category : #accessing }
EigtSubrGroup class >> call: aMethodName with: anEigtArgsList [
    | subr |
	subr := anEigtArgsList context findSubr: aMethodName asString.
	^ subr methodClass instanceSide
	    perform: subr selector
	    withArguments: {anEigtArgsList}.
]

{ #category : #accessing }
EigtSubrGroup class >> doNativeBinaryOperation: aList [
    "Perform a natively binary operation on an arbitrary number of operands."
    | accum message |
	aList size >= 3 ifFalse: [^ self error: 'Binary operation requires at lease 3 items.'].
    accum := aList second.
    message := aList first.
    3 to: aList size do: [ :idx | accum := accum perform: message withArguments: {(aList at: idx)} ].
    ^ accum.
]

{ #category : #accessing }
EigtSubrGroup class >> resolve: anEigtObject [
    
]

{ #category : #accessing }
EigtSubrGroup class >> sendToNative: aList [
    "Perform a Pharo-native operation."
    | receiver message args |
	aList size >= 2 ifFalse: [^ self error: 'A native call requires at lease 2 items.'].
    receiver := aList first.
    message := aList second.
    args := aList allButFirst: 2.
    ^ receiver perform: message withArguments: args.
]

{ #category : #accessing }
EigtSubrGroup >> = anObject [
    ^ self class = anObject class
    and: [self name = anObject name].
]

{ #category : #accessing }
EigtSubrGroup >> context [
    ^ context
]

{ #category : #accessing }
EigtSubrGroup >> context: anEigtContext [
    context := anEigtContext.
]

{ #category : #accessing }
EigtSubrGroup >> eq: anObject [
    ^ self class = anObject class
    and: [self name = anObject name].
]

{ #category : #accessing }
EigtSubrGroup >> initialize [
    meta := Dictionary new.
]

{ #category : #accessing }
EigtSubrGroup >> name [
    ^ meta at: 'name' ifAbsent: [nil].
]

{ #category : #accessing }
EigtSubrGroup >> name: aString [
    meta at: 'name' put: aString.
]

{ #category : #accessing }
EigtSubrGroup >> parent [
    ^ nil.
]

{ #category : #accessing }
EigtSubrGroup >> parent: anEigtSequence [
    nil.
]

{ #category : #accessing }
EigtSubrGroup >> print [
    ^ '#<subr ', (meta at: 'name'), '>'.
]

{ #category : #accessing }
EigtSubrGroup >> start [
    ^ nil.
]

{ #category : #accessing }
EigtSubrGroup >> start: _ [
    nil.
]

{ #category : #accessing }
EigtSubrGroup >> stop [
    ^ nil.
]

{ #category : #accessing }
EigtSubrGroup >> stop: _ [
    nil.
]
