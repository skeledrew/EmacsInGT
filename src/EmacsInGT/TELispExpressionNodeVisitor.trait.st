Trait {
	#name : #TELispExpressionNodeVisitor,
	#traits : 'TSmaCCParseNodeVisitor',
	#classTraits : 'TSmaCCParseNodeVisitor classTrait',
	#category : #'EmacsInGT-AST'
}

{ #category : #generated }
TELispExpressionNodeVisitor >> visitCharacter: aCharacter [
	^ self visitExpression: aCharacter
]

{ #category : #generated }
TELispExpressionNodeVisitor >> visitCons: aCons [
	^ self visitExpression: aCons
]

{ #category : #generated }
TELispExpressionNodeVisitor >> visitExpression: anExpression [
	^ self visitSmaCCParseNode: anExpression
]

{ #category : #generated }
TELispExpressionNodeVisitor >> visitFile: aFile [
	^ self visitExpression: aFile
]

{ #category : #generated }
TELispExpressionNodeVisitor >> visitFloat: aFloat [
	^ self visitExpression: aFloat
]

{ #category : #generated }
TELispExpressionNodeVisitor >> visitFunction: aFunction [
	^ self visitExpression: aFunction
]

{ #category : #generated }
TELispExpressionNodeVisitor >> visitHashTable: aHashTable [
	^ self visitExpression: aHashTable
]

{ #category : #generated }
TELispExpressionNodeVisitor >> visitInteger: anInteger [
	^ self visitExpression: anInteger
]

{ #category : #generated }
TELispExpressionNodeVisitor >> visitQuote: aQuote [
	^ self visitExpression: aQuote
]

{ #category : #generated }
TELispExpressionNodeVisitor >> visitRecord: aRecord [
	^ self visitExpression: aRecord
]

{ #category : #generated }
TELispExpressionNodeVisitor >> visitString: aString [
	^ self visitExpression: aString
]

{ #category : #generated }
TELispExpressionNodeVisitor >> visitSymbol: aSymbol [
	^ self visitExpression: aSymbol
]

{ #category : #generated }
TELispExpressionNodeVisitor >> visitUnquote: anUnquote [
	^ self visitExpression: anUnquote
]

{ #category : #generated }
TELispExpressionNodeVisitor >> visitVector: aVector [
	^ self visitExpression: aVector
]
