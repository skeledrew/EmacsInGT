Class {
	#name : #'OBSOLETE_BrEditorSelectWordAfterCursorShortcut',
	#superclass : #BrEditorShortcut,
	#category : #EmacsInGT
}

{ #category : #'api - combination' }
OBSOLETE_BrEditorSelectWordAfterCursorShortcut >> combinationForMacOS [
	^ combinationForMacOS
]

{ #category : #accessing }
OBSOLETE_BrEditorSelectWordAfterCursorShortcut >> description [
	^ 'Selects one word at a time to the right from the cursor.'
]

{ #category : #accessing }
OBSOLETE_BrEditorSelectWordAfterCursorShortcut >> initialize [
	super initialize.
	
	combination := BlKeyCombination shiftPrimaryArrowRight.
]

{ #category : #accessing }
OBSOLETE_BrEditorSelectWordAfterCursorShortcut >> name [
	^ 'Select word to the right'
]

{ #category : #accessing }
OBSOLETE_BrEditorSelectWordAfterCursorShortcut >> performOnEditor: aBrTextEditor element: aBrEditorElement dueTo: aShortcutEvent [
    | selecter startPos text endPos curPos hitNonWS |
    selecter := aBrTextEditor selecter.
    startPos := selecter cursors first position max: 1. "assume a single cursor"
    text := selecter editor text asString.
    startPos >= text size ifTrue: [^ self].
    endPos := DEPRECATED_ShortcutFactory findEndPosIn: text for: '\s' startingAt: startPos going: #right.
    selecter
		withoutCursorUpdate;
		moveBy: endPos - startPos;
	    select.
	aBrTextEditor navigator
		moveToEndNextWord;
		apply
]
