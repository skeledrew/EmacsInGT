Class {
	#name : #EigtEngineExamples,
	#superclass : #EigtBaseExampleClass,
	#instVars : [
		'repParams',
		'repllParams',
		'traceParams'
	],
	#category : #'EmacsInGT-Examples'
}

{ #category : #accessing }
EigtEngineExamples >> persistFunctionAlias [
    <gtExample>
    | path content engine result |
	path := 'tmpfile-{1}.el' format: {DateAndTime now asNanoSeconds}.
	content := '(defalias ''add ''+) (defalias ''sub ''-)'.
	[path asFileReference ensureCreateFile; writeStreamDo: [:s | s nextPutAll: content].
	engine := EigtEngine new.
	engine readPath: path; eval.
	result := engine read: '(sub 9 (add 5 1))'; eval; print.
	self assert: result = '3' description: 'Function alias eval failed'.
	engine := EigtEngine new.
	engine readPath: path; eval.] ensure:
	[path asFileReference ensureDelete.
	(path, '.fuel') asFileReference ensureDelete.].
	result := engine read: '(sub 9 (add 5 1))'; eval; print.
	self assert: result = '3' description: 'Function alias persistence failed'.
	^ engine.
]

{ #category : #accessing }
EigtEngineExamples >> readEvalPrint: anELispInputString andExpect: anELispOutputString [
    ""
    <gtExample>
    <parametrizeUsing: #repParams>
    | result |
	self getParamsFor: #repParams from: [
        GeaParameterSets new
            paramNames: #(anELispInputString anELispOutputString);
            + #('123' '123');
            + #('''123' '123');
            + #('(eval ''123)' '123');
            + #('(+ 1 3 5)' '9');
            + #('(setq a 123)' '123');
            + #('(setq a 123) (eval ''a)' '123');
            + #('(setq a 123) a' '123');
            + #('(symbol-function ''car)' '#<subr car>');  "ans is a symbol"
            + #('(fset ''first ''car)' 'car');
            + #('(fset ''first ''car) (first ''(1 2 3))' '1');
            + #('(funcall (lambda (a b c) (+ a b c)) 1 2 3)' '6');
            + #('(funcall ''list ''x ''y ''z)' '(x y z)');
            + #('(if nil (print ''true) ''very-false)' 'very-false');
            + #('(setq a 5) (cond ((eq a ''hack) ''foo) (t "default"))' '"default"');
            + #('(and (print 1) (print 2) nil (print 3))' 'nil');
            + #('(setq x 1) (or (eq x nil) (eq x 0))' 'nil');
            + #('(setq y 2) (let ((y 1) (z y)) (list y z))' '(1 2)');
            + #('(setq y 2) (let* ((y 1) (z y)) (list y z))' '(1 1)');
            + #('(prog1 1 2 3)' '1');
            + #('(prog2 1 2 3)' '2');
            + #('(progn 1 2 3)' '3');
            + #('(defalias ''add ''+) (add 2 1)' '3');
            + #('#''+' '+');
            + #('(function var)' 'var');
            + #('(apply ''+ 1 2 ''(3 4))' '10');
            + #('(mapcar ''car ''((a b) (c d) (e f)))' '(a c e)');
            + #('(mapcar ''1+ [1 2 3])' '(2 3 4)');
            + #('(mapcar ''string "abc")' '("a" "b" "c")');
            + #('(mapcar ''1+ "adr ")' '(98 101 115 33)');
            + #('(assq ''oak ''((pine . cones) (oak . acorns) (maple . seeds)))' '(oak . acorns)');
            + #('(put ''fly ''verb ''transitive) (get ''fly ''verb)' 'transitive');
            + #('`(contains ,(+ 1 2) elements)' '(contains 3 elements)');
            + #('`(1 2 (3 ,(+ 4 5)))' '(1 2 (3 9))');
            + #('(setq some-list ''(2 3)) `(1 ,@some-list 4 ,@some-list)' '(1 2 3 4 2 3)');
            + #('(setq list ''(hack foo bar)) `(use the words ,@(cdr list) as elements)' '(use the words foo bar as elements)');
            + #('(setq x 100) (defalias ''getx ''(lambda () x)) (let ((x 1)) (getx))' '1');
            + #('(defconst lexical-binding t) (setq x 100) (defalias ''getx ''(lambda () x)) (let ((x 1)) (getx))' '100');
            + #('(defconst lexical-binding t) (defvar my-ticker nil) (let ((x 0)) (setq my-ticker (lambda () (setq x (1+ x))))) (funcall my-ticker)' '1').
            
    ].
    result := EigtEngine new read: anELispInputString; eval; print.
    self assert: result = anELispOutputString description: '(eval ', anELispInputString, ')` is `', result, '`, not `', anELispOutputString, '`'.
    ^ result.
]

{ #category : #accessing }
EigtEngineExamples >> readEvalPrint: anELispInputString withLoadedLispAndExpect: anELispOutputString [
    "Examples requiring external lisp to be loaded."
    <gtExample>
    <parametrizeUsing: #repllParams>
    | result |
	self getParamsFor: #repllParams from: [
        GeaParameterSets new
            paramNames: #(anELispInputString anELispOutputString);
            + #('t' 't');  "ensure external lisp loads without error"
            + #('(defmacro inc (var) (list ''setq var (list ''1+ var))) (setq x 2) (inc x) x' '3');
            + #('(setq foo t) (defmacro t-becomes-nil (variable) `(if (eq ,variable t) (setq ,variable nil))) (t-becomes-nil foo) foo' 'nil');
            + #('(defun bar (a &optional b &rest c) (list a b c)) (bar 1 2 3 4 5)' '(1 2 (3 4 5))');
            + #('(setq x 100) (defun getx () x) (let ((x 1)) (getx))' '1');
            + #('(defconst lexical-binding t) (setq x 100) (defun getx () x) (let ((x 1)) (getx))' '100').
            
    ].
    result := EigtEngine new loadLisp; read: anELispInputString; eval; print.
    self assert: result = anELispOutputString description: '(eval ', anELispInputString, ')` is `', result, '`, not `', anELispOutputString, '`'.
    ^ result.
]

{ #category : #accessing }
EigtEngineExamples >> runTraceFrom: aStartBlock andAssertWith: anAssertBlock [
    <gtExample>
    <parametrizeUsing: #traceParams>
    | result  |
	self getParamsFor: #traceParams from: [
        GeaParameterSets new
            paramNames: #(aStartBlock anAssertBlock);
            + {
                [EigtEngine new read: '0'].
                [:res | res size = 1 and: [res first size = 1]].
            };
            + {
                [EigtEngine new read: '(setq a 5)'; eval; read: 'a'].
                [:res | (res first first variables get: #a) value = 5].
            };
            + {
                [EigtEngine new read: '(setq a 6)'; eval; read: '(1+ a)'].
                [:res | res size = 1 and: [res first size = 2]].
            };
            + {
                [EigtEngine new loadLisp; read: '(defvar lexical-binding t) (setq x 100) (defun getx () x) (let ((x 1)) (getx))'].
                [:res | false].
            }";
            + {
                [
                    | lispDirs |
                    lispDirs := Eigtils loadSimpleKVData at: #lispDirs.
                    EigtEngine new readPath: lispDirs.
                ].
                [:res | false].
            }".
    ].
    result := GeaTraceRunner new
        "setPreventStepIntoFilterTo: aPreventStepBlock;"
        setProcessClass: EigtDebugProcess;
        setStartPointTo: aStartBlock value "value first";
        setStopBlockTo: [:point | point node ~= false and: [point node innerNode isNil]];
        run.
    self assert: (anAssertBlock value: result) description: 'Failed on: ', aStartBlock asString.
    ^ result.
]

{ #category : #accessing }
EigtEngineExamples >> tracePersistFunctionAlias [
    <gtExample>
    | path content engine result |
	path := 'tmpfile-{1}.el' format: {DateAndTime now asNanoSeconds}.
	content := '(defalias ''add ''+) (defalias ''sub ''-)'.
	path asFileReference ensureCreateFile; writeStreamDo: [:s | s nextPutAll: content].
	engine := EigtEngine new.
	engine readPath: path.
	result := GeaTraceRunner new
        "setPreventStepIntoFilterTo: aPreventStepBlock;"
        setProcessClass: GeaPharoDebugProcess;
        setStartPointTo: [engine eval];
        "setStopBlockTo: [:point | point node ~= false and: [point node innerNode isNil]];"
        run.
	self assert: false description: 'Function alias persistence failed'.
	path asFileReference ensureDelete.
	(path, '.fuel') asFileReference ensureDelete.
	result := engine read: '(sub 9 (add 5 1))'; eval; print.
	self assert: result = '3' description: 'Function alias persistence failed'.
	^ engine.
]
