"
NOTE: Subclasses EigtVector as the docs says it behaves similarly https://www.gnu.org/software/emacs/manual/html_node/elisp/Records.html#Records
"
Class {
	#name : #EigtRecord,
	#superclass : #EigtVector,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtRecord class >> ast: anELispRecordNode [
    | inst aValue |
    aValue := anELispRecordNode value value.
    inst := EigtRecord new value: aValue; ast: anELispRecordNode.
	^ inst
]
