;;; byte-run.el --- byte-compiler support for inlining  -*- lexical-binding: t -*-

;; Excerpts from `emacs/26.3/lisp/emacs-lisp/byte-run.el'


(defalias 'function-put
  ;; We don't want people to just use `put' because we can't conveniently
  ;; hook into `put' to remap old properties to new ones.  But for now, there's
  ;; no such remapping, so we just call `put'.
  #'(lambda (function prop value)
      "Set FUNCTION's property PROP to VALUE.
The namespace for PROP is shared with symbols.
So far, FUNCTION can only be a symbol, not a lambda expression."
      (put function prop value)))
(function-put 'defmacro 'doc-string-elt 3)
(function-put 'defmacro 'lisp-indent-function 2)

(defvar macro-declaration-function #'macro-declaration-function
  "Function to process declarations in a macro definition.
The function will be called with two args MACRO and DECL.
MACRO is the name of the macro being defined.
DECL is a list `(declare ...)' containing the declarations.
The value the function returns is not used.")

(defalias 'macro-declaration-function
  #'(lambda (macro decl)
      "Process a declaration found in a macro definition.
This is set as the value of the variable `macro-declaration-function'.
MACRO is the name of the macro being defined.
DECL is a list `(declare ...)' containing the declarations.
The return value of this function is not used."
      ;; We can't use `dolist' or `cadr' yet for bootstrapping reasons.
      (let (d)
        ;; Ignore the first element of `decl' (it's always `declare').
        (while (setq decl (cdr decl))
          (setq d (car decl))
          (if (and (consp d)
                   (listp (cdr d))
                   (null (cdr (cdr d))))
              (cond ((eq (car d) 'indent)
                     (put macro 'lisp-indent-function (car (cdr d))))
                    ((eq (car d) 'debug)
                     (put macro 'edebug-form-spec (car (cdr d))))
                    ((eq (car d) 'doc-string)
                     (put macro 'doc-string-elt (car (cdr d))))
                    (t
                     (message "Unknown declaration %s" d)))
            (message "Invalid declaration %s" d))))))

(defvar defun-declarations-alist
  (list
   ;; We can only use backquotes inside the lambdas and not for those
   ;; properties that are used by functions loaded before backquote.el.
   (list 'advertised-calling-convention
         #'(lambda (f _args arglist when)
             (list 'set-advertised-calling-convention
                   (list 'quote f) (list 'quote arglist) (list 'quote when))))
   (list 'obsolete
         #'(lambda (f _args new-name when)
             (list 'make-obsolete
                   (list 'quote f) (list 'quote new-name) (list 'quote when))))
   (list 'interactive-only
         #'(lambda (f _args instead)
             (list 'function-put (list 'quote f)
                   ''interactive-only (list 'quote instead))))
   ;; FIXME: Merge `pure' and `side-effect-free'.
   (list 'pure
         #'(lambda (f _args val)
             (list 'function-put (list 'quote f)
                   ''pure (list 'quote val)))
         "If non-nil, the compiler can replace calls with their return value.
This may shift errors from run-time to compile-time.")
   (list 'side-effect-free
         #'(lambda (f _args val)
             (list 'function-put (list 'quote f)
                   ''side-effect-free (list 'quote val)))
         "If non-nil, calls can be ignored if their value is unused.
If `error-free', drop calls even if `byte-compile-delete-errors' is nil.")
   (list 'compiler-macro
         #'(lambda (f args compiler-function)
             (if (not (eq (car-safe compiler-function) 'lambda))
                 `(eval-and-compile
                    (function-put ',f 'compiler-macro #',compiler-function))
               (let ((cfname (intern (concat (symbol-name f) "--anon-cmacro"))))
                 `(progn
                    (eval-and-compile
                      (function-put ',f 'compiler-macro #',cfname))
                    ;; Don't autoload the compiler-macro itself, since the
                    ;; macroexpander will find this file via `f's autoload,
                    ;; if needed.
                    :autoload-end
                    (eval-and-compile
                      (defun ,cfname (,@(cadr compiler-function) ,@args)
                        ,@(cddr compiler-function))))))))
   (list 'doc-string
         #'(lambda (f _args pos)
             (list 'function-put (list 'quote f)
                   ''doc-string-elt (list 'quote pos))))
   (list 'indent
         #'(lambda (f _args val)
             (list 'function-put (list 'quote f)
                   ''lisp-indent-function (list 'quote val)))))
  "List associating function properties to their macro expansion.
Each element of the list takes the form (PROP FUN) where FUN is
a function.  For each (PROP . VALUES) in a function's declaration,
the FUN corresponding to PROP is called with the function name,
the function's arglist, and the VALUES and should return the code to use
to set this property.

This is used by `declare'.")

(defvar macro-declarations-alist
  (cons
   (list 'debug
         #'(lambda (name _args spec)
             (list 'progn :autoload-end
                   (list 'put (list 'quote name)
                         ''edebug-form-spec (list 'quote spec)))))
   (cons
    (list 'no-font-lock-keyword
          #'(lambda (name _args val)
              (list 'function-put (list 'quote name)
                    ''no-font-lock-keyword (list 'quote val))))
    defun-declarations-alist))
  "List associating properties of macros to their macro expansion.
Each element of the list takes the form (PROP FUN) where FUN is a function.
For each (PROP . VALUES) in a macro's declaration, the FUN corresponding
to PROP is called with the macro name, the macro's arglist, and the VALUES
and should return the code to use to set this property.

This is used by `declare'.")

(defalias 'defmacro
  (cons
   'macro
   #'(lambda (name arglist &optional docstring &rest body)
       "Define NAME as a macro.
When the macro is called, as in (NAME ARGS...),
the function (lambda ARGLIST BODY...) is applied to
the list ARGS... as it appears in the expression,
and the result should be a form to be evaluated instead of the original.
DECL is a declaration, optional, of the form (declare DECLS...) where
DECLS is a list of elements of the form (PROP . VALUES).  These are
interpreted according to `macro-declarations-alist'.
The return value is undefined.

\(fn NAME ARGLIST &optional DOCSTRING DECL &rest BODY)"
       ;; We can't just have `decl' as an &optional argument, because we need
       ;; to distinguish
       ;;    (defmacro foo (arg) (bar) nil)
       ;; from
       ;;    (defmacro foo (arg) (bar)).
       (let ((decls (cond
		     ((eq (car-safe docstring) 'declare)
		      (prog1 (cdr docstring) (setq docstring nil)))
		     ((and (stringp docstring)
			   (eq (car-safe (car body)) 'declare))
		      (prog1 (cdr (car body)) (setq body (cdr body)))))))
	 (if docstring (setq body (cons docstring body))
	   (if (null body) (setq body '(nil))))
	 ;; Can't use backquote because it's not defined yet!
	 (let* ((fun (list 'function (cons 'lambda (cons arglist body))))
		(def (list 'defalias
			   (list 'quote name)
			   (list 'cons ''macro fun)))
		(declarations
		 (mapcar
		  #'(lambda (x)
		      (let ((f (cdr (assq (car x) macro-declarations-alist))))
			(if f (apply (car f) name arglist (cdr x))
			  (message "Warning: Unknown macro property %S in %S"
				   (car x) name))))
		  decls)))
	   ;; Refresh font-lock if this is a new macro, or it is an
	   ;; existing macro whose 'no-font-lock-keyword declaration
	   ;; has changed.
	   (if (and
		;; If lisp-mode hasn't been loaded, there's no reason
		;; to flush.
		(fboundp 'lisp--el-font-lock-flush-elisp-buffers)
		(or (not (fboundp name)) ;; new macro
		    (and (fboundp name)  ;; existing macro
			 (member `(function-put ',name 'no-font-lock-keyword
						',(get name 'no-font-lock-keyword))
				 declarations))))
	       (lisp--el-font-lock-flush-elisp-buffers))
	   (if declarations
	       (cons 'prog1 (cons def declarations))
	     def))))))

;; Now that we defined defmacro we can use it!
(defmacro defun (name arglist &optional docstring &rest body)
  "Define NAME as a function.
The definition is (lambda ARGLIST [DOCSTRING] BODY...).
See also the function `interactive'.
DECL is a declaration, optional, of the form (declare DECLS...) where
DECLS is a list of elements of the form (PROP . VALUES).  These are
interpreted according to `defun-declarations-alist'.
The return value is undefined.

\(fn NAME ARGLIST &optional DOCSTRING DECL &rest BODY)"
  ;; We can't just have `decl' as an &optional argument, because we need
  ;; to distinguish
  ;;    (defun foo (arg) (toto) nil)
  ;; from
  ;;    (defun foo (arg) (toto)).
  (declare (doc-string 3) (indent 2))
  (or name (error "Cannot define '%s' as a function" name))
  (if (null
       (and (listp arglist)
            (null (delq t (mapcar #'symbolp arglist)))))
      (error "Malformed arglist: %s" arglist))
  (let ((decls (cond
                ((eq (car-safe docstring) 'declare)
                 (prog1 (cdr docstring) (setq docstring nil)))
                ((and (stringp docstring)
		      (eq (car-safe (car body)) 'declare))
                 (prog1 (cdr (car body)) (setq body (cdr body)))))))
    (if docstring (setq body (cons docstring body))
      (if (null body) (setq body '(nil))))
    (let ((declarations
           (mapcar
            #'(lambda (x)
                (let ((f (cdr (assq (car x) defun-declarations-alist))))
                  (cond
                   (f (apply (car f) name arglist (cdr x)))
                   ;; Yuck!!
                   ((and (featurep 'cl)
                         (memq (car x)  ;C.f. cl-do-proclaim.
                               '(special inline notinline optimize warn)))
                    (push (list 'declare x)
                          (if (stringp docstring)
                              (if (eq (car-safe (cadr body)) 'interactive)
                                  (cddr body)
                                (cdr body))
                            (if (eq (car-safe (car body)) 'interactive)
                                (cdr body)
                              body)))
                    nil)
                   (t (message "Warning: Unknown defun property `%S' in %S"
                               (car x) name)))))
                   decls))
          (def (list 'defalias
                     (list 'quote name)
                     (list 'function
                           (cons 'lambda
                                 (cons arglist body))))))
      (if declarations
          (cons 'prog1 (cons def declarations))
          def))))

;; NOTE: This `lambda' definition belongs in `subr.el', but easier to put here for testing; currently causes infinite recursion

;; (defmacro lambda (&rest cdr)
;;   "Return an anonymous function.
;; Under dynamic binding, a call of the form (lambda ARGS DOCSTRING
;; INTERACTIVE BODY) is self-quoting; the result of evaluating the
;; lambda expression is the expression itself.  Under lexical
;; binding, the result is a closure.  Regardless, the result is a
;; function, i.e., it may be stored as the function value of a
;; symbol, passed to `funcall' or `mapcar', etc.

;; ARGS should take the same form as an argument list for a `defun'.
;; DOCSTRING is an optional documentation string.
;;  If present, it should describe how to call the function.
;;  But documentation strings are usually not useful in nameless functions.
;; INTERACTIVE should be a call to the function `interactive', which see.
;; It may also be omitted.
;; BODY should be a list of Lisp expressions.

;; \(fn ARGS [DOCSTRING] [INTERACTIVE] BODY)"
;;   (declare (doc-string 2) (indent defun)
;;            (debug (&define lambda-list lambda-doc
;;                            [&optional ("interactive" interactive)]
;;                            def-body)))
;;   ;; Note that this definition should not use backquotes; subr.el should not
;;   ;; depend on backquote.el.
;;   (list 'function (cons 'lambda cdr)))
