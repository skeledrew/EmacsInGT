Class {
	#name : #BaselineOfEmacsInGT,
	#superclass : #BaselineOf,
	#category : #BaselineOfEmacsInGT
}

{ #category : #accessing }
BaselineOfEmacsInGT >> baseline: spec [
    <baseline>
    spec for: #common do: [
        spec
            baseline: 'GtExemplifierAdditions' with: [
                spec repository: 'gitlab://skeledrew/GtExemplifierAdditions:master/src'];
            package: 'EmacsInGT' with: [
                spec requires: #(GtExemplifierAdditions)].
    ]
]
